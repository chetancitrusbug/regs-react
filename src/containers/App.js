import React, { Component } from 'react';
// import './App.css';
import '../components/css/bootstrap.min.css'
// import '../components/css/font-awesome.min.css'
import '../components/css/icons.css'
import '../components/css/style.css'
import Routes from './routes';

class App extends Component {
  constructor(){
    super();
    this.state={
      appName: "RegEx",
      home: false
    }
  }
  render() {
    return (
      
      <div className="App">
        <Routes name={this.state.appName}/>
      </div>
      
    );
  }
}

export default App;
