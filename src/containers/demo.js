import React from 'react';
import ReactDOM from 'react-dom';
import InputRange from 'react-input-range';
//import 'react-input-range/lib/css/index.css'
//import '../components/css/development.css'
class Demo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: { min: 100, max: 2000 },
        };
    }

    render() {
        return (
            <InputRange
                maxValue={10000}
                minValue={0}
                formatLabel={value => `${value}`}
                value={this.state.value}
                onChange={value => this.setState({ value })} />
        );
    }
}

export default Demo;




// import React, { Component } from "react";
// import axios from 'axios'
// import { CustomerData } from '../components/services/PostData';
// const style = {
//     height: 330,
//     border: "1px solid green",
//     margin: 6,
//     padding: 8

// };
// class Demo extends Component {

//     constructor(props) {
//         super(props);
//         this.state = {
//             users: [],
//             page: 1,
//             loading: false,
//             prevY: 0
//         };
//     }

//     handleObserver(entities, observer) {
//         const y = entities[0].boundingClientRect.y;
//         if (this.state.prevY > y) {
//             const lastUser = this.state.users[this.state.users.length - 1];
//             const curPage = this.state.page +1;
//             this.getUsers(curPage);
//             this.setState({ page: curPage });
//         }
//         this.setState({ prevY: y });
//     }

//     getUsers(page) {
//         this.setState({ loading: true });
//         // axios
//         //     .get(`https://api.github.com/users?since=${page}&per_page=5`)
//         //     .then(res => {
//         //         this.setState({ users: [...this.state.users, ...res.data] });
//         //         this.setState({ loading: false });
//         //     });
//         if (sessionStorage.getItem('userData')) {
//             let userData = JSON.parse(sessionStorage.getItem('userData'));
//             let postData = {
//                 'api_token': userData.api_token,
//                 'user_id': userData.id,
//                 'filter': '',
//                 'search': this.state.search,
//                 'page': page

//             };
//             CustomerData(postData).then((result) => {
//                 if (result.data.status == true) {
//                     this.setState({ users: [...this.state.users, ...result.data.customerList.data] });
//                     //this.setState({ customerList: result.data.customerList });
//                     this.setState({ loading: false });
//                 }
//                 else {
//                     sessionStorage.removeItem('userData');
//                     this.setState({ login: false });
//                 }
//             });
//             console.log(this.state);
//         }
//     }

//     componentDidMount() {
//         this.getUsers(this.state.page);

//         // Options
//         var options = {
//             root: null, // Page as root
//             rootMargin: '0px',
//             threshold: 1.0
//         };
//         // Create an observer
//         this.observer = new IntersectionObserver(
//             this.handleObserver.bind(this), //callback
//             options
//         );
//         //Observ the `loadingRef`
//         this.observer.observe(this.loadingRef);
//     }

//     render() {
//         const loadingCSS = {
//             height: '100px',
//             margin: '30px'
//         };
//         const loadingTextCSS = { display: this.state.loading ? 'block' : 'none' };
//         return (
//             <div className="container">
//                 <div style={{ minHeight: '800px' }}>
//                     <ul>
//                         {this.state.users.map(user => <li style={style} key={user.id}>{user.first_name}</li>)}
//                     </ul>
//                 </div>
//                 <div
//                     ref={loadingRef => (this.loadingRef = loadingRef)}
//                     style={loadingCSS}
//                 >
//                     <span style={loadingTextCSS}>Loading...</span>
//                 </div>
//             </div>
//         );
//     }





//     // constructor(props) {
//     //     super(props);
//     //     this.state = {
//     //         items: Array.from({ length: 10 }),
//     //         height: '' 
//     //     }
//     // }
    
//     // componentWillMount() {
//     //     this.setState({ height: window.innerHeight });
//     // }
//     // handleScroll(){
//     //     console.log(this.scroller);
//     //     if (this.scroller){
//     //         this.setState({
//     //             items: this.state.items.concat(Array.from({ length: 20 })),
//     //             height: window.innerHeight
//     //         });
//     //         console.log(this.state);
//     //     }
//     //     console.log(this.state);
//     // }
//     // render() {
        
//     //     return (
//     //         <div 
//     //             className="loginForm" 
//     //             onScroll={this.handleScroll()}
//     //             ref = {(scroller) => {
//     //                 this.scroller = scroller;
//     //             }}
            
//     //         >
//     //             {this.state.items.map((i, index) => (
//     //                 <div style={style} key={index}>
//     //                     div - #{index}
//     //                 </div>
//     //             ))}
//     //         </div>
//     //     )
//     // }
// }

// export default Demo;
