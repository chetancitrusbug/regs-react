import React from 'react';
import {BrowserRouter, Route,  Switch} from 'react-router-dom';
// import Demo from './demo';
import Login from '../components/auth/Login';
import ForgotPassword from '../components/auth/ForgotPassword';
import ChangePassword from '../components/user/ChangePassword';
//import Profile from '../components/user/Profile';
import Home from '../components/userModules/Home'
import Upload from '../components/userModules/Upload'
import Insights from '../components/userModules/Insights'
import AccuracyReport from '../components/userModules/AccuracyReport'
import AccuracyInstrument from '../components/userModules/AccuracyInstrument'
import FirmArm from '../components/userModules/FirmArm'
import FirmArmNon from '../components/userModules/FirmArmNon'
import DataImport from '../components/userModules/DataImport'
import Demo from './demo'

const Routes = () => (
<BrowserRouter >
    <Switch>
        {/* <Route exact path="/" component={Welcome}/> */}
            <Route path="/" exact component={Login}/>
        <Route exact path="/home" component={Home} />
        {/* <Route exact path="/demo" component={Demo} /> */}
            <Route path="/login" exact  component={Login}/>
            <Route path="/insights" exact component={Insights} />
            <Route path="/accuracy-report" exact component={AccuracyReport} />
            <Route path="/firm-arm" exact component={FirmArm} />
            <Route path="/firm-arm-nca" exact component={FirmArmNon} />
            <Route path="/accuracy-instrument" exact component={AccuracyInstrument} />
            <Route path="/data-import" exact component={DataImport} />
            <Route path="/upload" exact  component={Upload}/>
        <Route path="/forgot-password" component={ForgotPassword}/>
        <Route path="/change-password" component={ChangePassword} />
        {/* <Route path="/profile" component={Profile} /> */}
        {/* <Route path='/customer/edit/:empid' component={CustomerEdit} /> 
        <Route path='/customer/view/:empid' component={CustomerView} /> 
        <Route path='/offer/create/:custid' component={OfferCreate} />  */}
        {/* <Route path="*" component={NotFound}/> */}
            
    </Switch>
</BrowserRouter>
);
export default Routes;