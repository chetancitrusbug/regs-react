import React, { Component } from 'react';

import { Redirect } from 'react-router-dom';
import FormErrors from '../error/FormErrors'
import { ChangePasswordData } from '../services/PostData';

class ChangePassword extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            oldpassword: '',
            newpassword: '',
            confirmNewPassword: '',
            redirectToReferrer: false,
            formErrors: { email: '', oldpassword: '', newpassword: '', confirmNewPassword: ''},
            emailValid: false,
            oldPasswordValid: false,
            newPasswordValid: false,
            confirmNewPasswordValid: false,
            formValid: false,
            cp: false
            
        };
        this.changePassword = this.changePassword.bind(this);
    }
    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let oldPasswordValid = this.state.oldPasswordValid;
        let newPasswordValid = this.state.newPasswordValid;
        let confirmNewPasswordValid = this.state.confirmNewPasswordValid;

        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' is invalid';
                break;
            // case 'password':
            //     passwordValid = value.length >= 6;
            //     fieldValidationErrors.password = passwordValid ? '' : ' is too short';
            //     break;
            case 'oldpassword':
                oldPasswordValid = value.length >= 6;
                fieldValidationErrors.oldpassword = oldPasswordValid ? '' : ' is too short';
                break;
            case 'newpassword':
                newPasswordValid = value.length >= 6;
                fieldValidationErrors.newpassword = newPasswordValid ? '' : ' is too short';
                break;
            case 'confirmNewPassword':
                confirmNewPasswordValid = value.length >= 6;
                if (!confirmNewPasswordValid){
                    fieldValidationErrors.confirmNewPassword =  ' is too short';
                }else{
                    
                    if (value === this.state.newpassword){
                        fieldValidationErrors.confirmNewPassword = '';
                    }else{
                        fieldValidationErrors.confirmNewPassword = ' does not match with new pasword';
                    }
                }
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid,
            oldPasswordValid: oldPasswordValid,
            newPasswordValid: newPasswordValid,
            confirmNewPasswordValid: confirmNewPasswordValid
            
        }, this.validateForm);
    }
    
    handleUserInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value },
            () => { this.validateField(name, value) });
    }

    validateForm() {
        this.setState({ formValid: this.state.emailValid && this.state.oldPasswordValid && this.state.newPasswordValid && this.state.confirmNewPassword});
    }
    errorClass(error) {
        return (error.length === 0 ? '' : 'has-error');
    }
    changePassword(){
        if (sessionStorage.getItem('userData')) {
            let userData = JSON.parse(sessionStorage.getItem('userData'));
            //console.log(this.state);
            let postData = {
                'api_token': userData.api_token,
                'user_id': userData.id,
                'old_password': this.state.oldpassword,
                'password': this.state.newpassword,
                'password_confirmation': this.state.confirmNewPassword
            };
            ChangePasswordData(postData)
                .then((result) => {
                    let responseJson = result;
                    if (responseJson.status === false) {
                        let fieldValidationErrors = this.state.formErrors;
                        fieldValidationErrors.email = responseJson.message;
                        this.setState({
                            formErrors: fieldValidationErrors,
                            emailValid: false,
                        }, this.validateForm);
                    } else {
                        sessionStorage.removeItem('userData');
                        alert(responseJson.message)
                        this.setState({ fp: true });
                    }
                });
        }
        
    }
    render() {
        if (!sessionStorage.getItem('userData')) {
            return <Redirect exact push to={{ pathname: '/' }} />
        }
        if (this.state.fp) {
            return <Redirect exact push to={{ pathname: '/' }} />
        }
        return (
            <div className="loginForm">
                <div className="panel panel-default">
                    <FormErrors formErrors={this.state.formErrors} />
                </div>

                <div className="demoForm">
                    <h2>Change Password</h2>
                    <div className={`form-group col-md-12 ${this.errorClass(this.state.formErrors.email)}`}>
                        <label className="col-md-6" htmlFor="email">Email address</label>
                        <input type="email" onChange={(event) => this.handleUserInput(event)} className=" form-control" value={this.state.email} name="email" />
                    </div>
                    <div className={`form-group col-md-12 ${this.errorClass(this.state.formErrors.oldpassword)}`}>
                        <label htmlFor="password" className="col-md-6">Old Password</label>
                        <input type="password" onChange={(event) => this.handleUserInput(event)} className="form-control " value={this.state.oldpassword} name="oldpassword" />
                    </div>
                    <div className={`form-group col-md-12 ${this.errorClass(this.state.formErrors.newpassword)}`}>
                        <label htmlFor="password" className="col-md-6">New Password</label>
                        <input type="password" onChange={(event) => this.handleUserInput(event)} className="form-control " value={this.state.newpassword} name="newpassword" />
                    </div>
                    <div className={`form-group col-md-12 ${this.errorClass(this.state.formErrors.confirmNewPassword)}`}>
                        <label htmlFor="password" className="col-md-6">Confirm New Password</label>
                        <input type="password" onChange={(event) => this.handleUserInput(event)} className="form-control " value={this.state.confirmNewPassword} name="confirmNewPassword" />
                    </div>
                    <div className="col-md-9">
                        <button type="submit" onClick={this.changePassword} className="btn btn-primary ">
                            Submit
                        </button>
                    </div>
                </div>

            </div>
        );
    }
}
export default ChangePassword;