import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import api from '../api.js'
import Logo from '../images/logo.png'
import axios, { post } from 'axios'
class Login extends Component {
    constructor(){
        super();
        this.state = {
            email: '',
            password: '',
            redirectToReferrer: false,
            formErrors: {email: '', password: ''},
            emailValid: false,
            passwordValid: false,
            formValid: false
        };
        this.login = this.login.bind(this);
       // this.onChange = this.onChange.bind(this);
        
    }
    handleUserInput (e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value}, 
                    () => { this.validateField(name, value) });
    }

    login() {
        if(this.state.email && this.state.password){
            let user = {
                email: this.state.email,
                password: this.state.password,
            };
            
            axios.request({
                method: "post",
                url: api.url+api.login,
                crossDomain: true,
                data: user
            })
             .then((result) => {
                let responseJson = result;
                if(responseJson.data){
                    console.log(responseJson);
                    console.log(responseJson.data.status);
                    if (responseJson.data.status != "false"){
                        sessionStorage.setItem('userData', JSON.stringify(responseJson.data.result));
                        //sessionStorage.setItem('settings', JSON.stringify(responseJson.data.data.settings));
                        this.setState({redirectToReferrer: true});
                    }else{
                        alert(responseJson.data.message);
                    }
                }
            });
            //this.setState({redirectToReferrer: true});
        }
    }

    

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;

        switch(fieldName) {
            // case 'email':
            //     emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
            //     fieldValidationErrors.email = emailValid ? '' : ' is invalid';
            //     break;
            case 'password':
                passwordValid = value.length >= 3;
                fieldValidationErrors.password = passwordValid ? '': ' is too short';
                break;
            default:
                break;
        }
        this.setState({formErrors: fieldValidationErrors,
                        emailValid: emailValid,
                        passwordValid: passwordValid
                        }, this.validateForm);
        }

    validateForm() {
        this.setState({ formValid: this.state.emailValid && this.state.passwordValid});
    }
    errorClass(error) {
        return(error.length === 0 ? '' : 'has-error');
    }
    render() {
        if (this.state.redirectToReferrer || sessionStorage.getItem('userData')) {
            return <Redirect exact push to={{ pathname: '/insights' }} />
        }
        return (
            // <div className="loginForm"> 
            //     <div className="panel panel-default">
            //         <FormErrors formErrors={this.state.formErrors} />
            //     </div>
            //     <div className="demoForm">
            //         <h2>Sign In</h2>
            //         <div className={`form-group col-md-12 ${this.errorClass(this.state.formErrors.email)}`}>
            //             <label className="col-md-6" htmlFor="email">Email address</label>
            //             <input type="email" onChange={(event) => this.handleUserInput(event)} className=" form-control" value={this.state.email}  name="email"  />
            //         </div>
            //         <div className={`form-group col-md-12 ${this.errorClass(this.state.formErrors.password)}`}>
            //             <label htmlFor="password" className="col-md-6">Password</label>
            //             <input type="password" onChange={(event) => this.handleUserInput(event)} className="form-control "  value={this.state.password} name="password" />
            //         </div>
            //         <div className="col-md-9">
            //             <button type="submit" onClick={this.login} className="btn btn-primary ">
            //                 Sign In
            //             </button>
            //         </div>
                    
            //         {/* <div className="cols col-md-3">
            //             <a href="/forgot-password" className="btn btn-primary">Forgot Password</a>
            //         </div> */}
                   
            //     </div>
                
            // </div>
            <div className="wrapper-page">
                <div className="card">
                    <div className="card-body">
                        <h3 className="text-center m-0">
                            <a href="insights.html" className="logo logo-admin">
                                <img src={Logo} alt="RegX" />
                            </a>
                        </h3>
                        <div className="p-3">
                            <h4 className="text-muted font-18 m-b-5 text-center">Welcome Back !</h4>
                            <p className="text-muted text-center">Sign in to continue to Regx.</p>
                            <div className="form-horizontal m-t-30">
                                <div className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
                                    <label htmlFor="username">Username</label> 
                                    <input type="text" name="email" onChange={(event) => this.handleUserInput(event)} value={this.state.email} className="form-control" id="username" placeholder="Enter username" />
                                </div>
                                <div className={`form-group ${this.errorClass(this.state.formErrors.password)}`}>
                                    <label htmlFor="userpassword">Password</label> 
                                    <input type="password" name="password" onChange={(event) => this.handleUserInput(event)} value={this.state.password} className="form-control" id="userpassword" placeholder="Enter password"/>
                                </div>
                                <div className="form-group row m-t-20">
                                    <div className="col-6">
                                        <div className="custom-control custom-checkbox">
                                            <input type="checkbox" className="custom-control-input" id="customControlInline" /> 
                                            <label className="custom-control-label" htmlFor="customControlInline">Remember me</label>
                                        </div>
                                    </div>
                                    <div className="col-6 text-right">
                                        <button className="btn btn-primary w-md waves-effect waves-light" onClick={this.login} type="submit">Log In</button>
                                    </div>
                                </div>
                                <div className="form-group m-t-10 mb-0 row">
                                    <div className="col-12 m-t-20">
                                        <a href="/forgot-password" className="text-muted">
                                            <i className="mdi mdi-lock"></i> Forgot your password?
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="m-t-40 text-center">
                    <p>&copy; 2018 Regx.</p>
                </div>
            </div>
        );
    }
}
export default Login;