import React, { Component } from 'react';
// import '../css/login.css';
import { Redirect } from 'react-router-dom';
import { ForgotPasswordData } from '../services/PostData';

class ForgotPassword extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            redirectToReferrer: false,
            formErrors: { email: '', password: '' },
            emailValid: false,
            formValid: false,
            fp : false
        };
        this.Forgotpassword = this.Forgotpassword.bind(this);
        // this.onChange = this.onChange.bind(this);

    }
    handleUserInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value },
            () => { this.validateField(name, value) });
    }

    Forgotpassword() {
        if (this.state.email ) {
            let user = {
                email: this.state.email,
            };
            ForgotPasswordData(user)
                .then((result) => {
                    let responseJson = result;
                    if (responseJson.status === false) {
                        let fieldValidationErrors = this.state.formErrors;
                        fieldValidationErrors.email = responseJson.message;
                        this.setState({
                            formErrors: fieldValidationErrors,
                            emailValid: false,
                        }, this.validateForm);
                    } else {
                        alert('Please Check your Email ' + responseJson.message);
                        this.setState({ fp: true });
                    }
                });
        }
    }



    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        
        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' is invalid';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid,
        }, this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.emailValid  });
    }
    errorClass(error) {
        return (error.length === 0 ? '' : 'has-error');
    }
    render() {
        if(sessionStorage.getItem('userData')) {
            return <Redirect exact push to={{ pathname: '/' }} />
        }
        if (this.state.fp) {
            return <Redirect exact push to={{ pathname: '/' }} />
        }
        return (
            <div className="loginForm">
               
                    {/* <FormErrors formErrors={this.state.formErrors} /> */}
                {this.state.formErrors.email.length > 0 && ( <div className="panel panel-default"> <div className='formErrors'>
                        <p key={this.state.formErrors}>{this.state.formErrors.email}</p>
                </div>  </div> )  }
               
                <div className="demoForm">
                    <h2>Forgot Password</h2>
                    <div className={`form-group col-md-12 ${this.errorClass(this.state.formErrors.email)}`}>
                        <label className="col-md-6" htmlFor="email">Email address</label>
                        <input type="email" onChange={(event) => this.handleUserInput(event)} className=" form-control" value={this.state.email} name="email" />
                    </div>
                    <div className="col-md-9">
                        <button type="submit" onClick={this.Forgotpassword} className="btn btn-primary ">
                            Submit
                        </button>
                    </div>

                    <div className="cols col-md-3">
                        <a href="/" className="btn btn-primary">Sign In</a>
                    </div>

                </div>

            </div>
        );
    }
}
export default ForgotPassword;