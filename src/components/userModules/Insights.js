import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Header from '../navigation/Header';
import Footer from '../navigation/Footer';
//import { Line, Bar } from 'react-chartjs-2';
import axios, { post }from 'axios'
class Insights extends Component {
    constructor() {
        super();
        this.state = {
        };
    }
    


    
    render(){
        let chartData = {
            labels: ['A', 'B', 'C', 'D'],
            datasets: [
                {
                    backgroundColor: ["#dbdee7", "#dbdee7", "#00d563", "#dbdee7"],
                    borderColor: ["#dbdee7", "#dbdee7", "#00d563", "#dbdee7"],
                    borderWidth: 1,
                    data: [1, 10, 12, 15],
                },

            ],
        };
        return (
            <div >
                <Header activeClass="insights" />
                <div className="wrapper">
                    <div className="container-fluid">
                        {/* <!-- Page-Title --> */}
                        <div className="row">
                            <div className="col-sm-6 col-xl-6">
                                <div className="page-title-box">
                                    <h4 className="page-title">Insights</h4>
                                </div>
                            </div>

                                                
                            <div className="col-sm-6 col-xl-6 pull-right">
                                <div className="right-blk-div">
                                    <select>
                                        <option>Firm-ARM Recs insight</option>
                                        <option>Firm-ARM-NCA Recs Insight</option>
                                        <option>ARM-FIRDS ISINs Insight</option>
                                        <option>ARM-Vendor Instrument Data Insight</option>
                                        <option>ARM-Vendor Pricing Data Insight</option>
                                        <option>ARM-Firm-Personnel Data Insight</option>
                                    </select>
                                </div>
                                <div className="right-blk-div">
                                    <div>
                                        <span>Download: 
                                            <a href="#"><i className="fas fa-file-pdf"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="container-fluid">
                    
                        <div className="row">
                            <div className="col-xl-4">
                                <div className="card m-b-20">
                                    <div className="card-body">
                                        <h4 className="mt-0 header-title">Latest Rec Stats</h4>
                                        <div className="row text-center m-t-20">
                                            <div className="col-6">
                                                <h5 className="">$56241</h5>
                                                <p className="text-muted">Marketplace</p>
                                            </div>
                                            <div className="col-6">
                                                <h5 className="">$23651</h5>
                                                <p className="text-muted">Total Income</p>
                                            </div>
                                        </div>
                                        <div id="morris-donut-example" className="morris-chart-height"></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-8">
                                <div className="card m-b-20">
                                    <div className="card-body">
                                        <h4 className="mt-0 header-title">Weekly trend of tickets received, resolved and unresovled</h4>
                                        <div className="row text-center m-t-20">
                                            <div className="col-4">
                                                <h5 className="">$ 89425</h5>
                                                <p className="text-muted">Marketplace</p>
                                            </div>
                                            <div className="col-4">
                                                <h5 className="">$ 56210</h5>
                                                <p className="text-muted">Total Income</p>
                                            </div>
                                            <div className="col-4">
                                                <h5 className="">$ 8974</h5>
                                                <p className="text-muted">Last Month</p>
                                            </div>
                                        </div>
                                        {/* <div id="morris-bar-example" className="morris-chart-height"></div> */}
                                        {/* <Bar
                                            className="quizeChart"
                                            data={chartData}
                                            height={200}
                                            options={this.chartOptions}
                                        /> */}
                                    </div>
                                </div>
                            </div>
                    
                        </div>
                    {/* <!-- end row --> */}
                    
                    </div>
                    {/* <!-- end container-fluid --> */}
                </div>
            {/* <!-- end wrapper --><!-- Footer --> */}
                <Footer/>
            </div>

            
        )
    }

}

export default Insights;