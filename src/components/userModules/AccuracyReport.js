import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Header from '../navigation/Header';
import Footer from '../navigation/Footer';
import Filter from '../navigation/Filter';
// import { Line, Bar } from 'react-chartjs-2';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios, { post }from 'axios'
import api from '../api.js'
class AccuracyReport extends Component {
    constructor() {
        super();
        var tempDate = new Date();
        var date = tempDate.getFullYear() + '-' + (tempDate.getMonth() + 1) + '-' + tempDate.getDate();
        console.log("Current Date= " + date);
        this.state = {
            recouncilationdata : [],
            recouncilationColumns : [],
            userData: JSON.parse(sessionStorage.getItem('userData')),
            loading: false,
            ARMReportedLoading: false,
            date:date,
        };  
        this.handleChange = this.handleChange.bind(this);
    }
    
    handelList = () => {
        this.setState({ ARMReportedLoading: false });
        let url = api.url + api.getRecouncilation + '?api_token=' + this.state.userData.API_Token + '&clientName=' + this.state.userData.ClientName + '&ReconciliationType=firm-arm&RecAttribute=RecordMatching&date=' + this.state.date;

        axios.get(url).then(response => {
           
            this.setState({
                recouncilationdata: response.data.result.recouncilation,
                recouncilationColumns: response.data.result.columns,
            });
        }).catch(error => {
            console.log(error);
        });
        this.setState({ ARMReportedLoading: true });
    }

    handleChange(date) {
        var tempDate = new Date(date);
        var date = tempDate.getFullYear() + '-' + (tempDate.getMonth() + 1) + '-' + tempDate.getDate();
        console.log("Current Date= " + date);
        this.setState({
            date: date
        });
        this.handelList()
        
    }

    componentWillMount() {
       
        this.setState({ loading: false });
        console.log(this.state)
        this.handelList()
        this.setState({ loading: true });
       
        
    }
    
    columnsRecouncilation(columnsRecouncilationmap){
        let columns=[];
        columns.push(
            <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
        )
        Object.keys(columnsRecouncilationmap).map(function (data, number) {
            columns.push(<td>{columnsRecouncilationmap[data]}</td>)
        });
        return columns;
    }

    generateRows = (recouncilationdata) => {
        const rows = [];
        for (let i = 0; i < recouncilationdata.length; i++) {
            rows.push(
                <tr key={`row_${i}`} >
                    {this.columnsRecouncilation(recouncilationdata[i])}
                </tr>
            );
        }
        
        return rows;
    }
    render(){
        let chartData = {
            labels: ['A', 'B', 'C', 'D'],
            datasets: [
                {
                    backgroundColor: ["#dbdee7", "#dbdee7", "#00d563", "#dbdee7"],
                    borderColor: ["#dbdee7", "#dbdee7", "#00d563", "#dbdee7"],
                    borderWidth: 1,
                    data: [1, 10, 12, 15],
                },

            ],
        };

        // var allClients = this.state.recouncilationColumns.map(function (data, number) {
        //     return <div>{data}</div>
        // });
        var tableHeadwithColumns;
        let columns =  this.state.recouncilationColumns;
        if (!(columns.length == 0)){
            tableHeadwithColumns = columns.map(function (data, number) {
                return <th>{data}</th>
            });
        }
        

        return (
            <div >
                {!this.state.loading ? <div><i className="fa fa-spin fa-fw fa-spinner"></i> Loding</div> :
                <div>
                <Header activeClass="accuracyReport" />
                <div className="wrapper">
			        <div className="container-fluid">
					{/* <!-- Page-Title --> */}
					    <div className="row">
					        <div className="col-xl-6 col-sm-6">
                                <div className="page-title-box">
							        <h4 className="page-title">MiFD II - Accuracy > Reporting Eligibility</h4>
                                </div>
                            </div>
                            <Filter />
                        </div>
				    </div>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-xl-12">
                                
                                <div className="card m-b-20">
                                    <div className="card-body">
                                                
                                        <h4 className="mt-0 m-b-10 header-title float-left">ARM Reported ISINs</h4>
                                            <div className="col-xl-12 datepickerArm">
                                                    <DatePicker className=""
                                                        selected={this.state.date}
                                                        onChange={this.handleChange}
                                                        dateFormat="d-M-y"
                                                    />
                                            </div>
                                        {!this.state.ARMReportedLoading ? <div><i className="fa fa-spin fa-fw fa-spinner"></i> Loding</div> :
                                            <div className="table-responsive">
                                                <table className="fixed_header table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <div className="checkbox-wrapper">
                                                                    <input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label>
                                                                </div>
                                                            </th>
                                                            {tableHeadwithColumns}
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {this.generateRows(this.state.recouncilationdata)}
                                                    </tbody>
                                                </table>
                                            </div>
                                        }
                                        <h4 className="mt-0 m-b-10 header-title float-left">FIRDS ISINs</h4>
                                        <div className="table-responsive">
                                            <table className="fixed_header table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></th>
                                                        <th>Date</th>
                                                        <th>Chk</th>
                                                        <th>Col 3</th>
                                                        <th>Col 4</th>
                                                        <th>Col 5</th>
                                                        <th>Col 6</th>
                                                        <th>Col 7</th>
                                                        <th>Col 8</th>
                                                        <th>Col 9</th>
                                                        <th>Col 10</th>
                                                        <th>Col 11</th>
                                                        <th>Col 12</th>
                                                        <th>Col 13</th>
                                                        <th>Col 14</th>
                                                        <th>Col 15</th>
                                                        <th>Col 16</th>
                                                        <th>Col 17</th>
                                                        <th>Col 18</th>
                                                        <th>Col 19</th>
                                                        <th>Col 20</th>
                                                        <th>Col 21</th>
                                                        <th>Col 22</th>
                                                        <th>Col 23</th>
                                                        <th>Col 24</th>
                                                        <th>Col 25</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>     <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>                        
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        {/* <!-- end of table responsive --> */}
                                    </div>
                                </div>
                            </div>
                        </div>               
            {/* <!-- end row --> */}
                    </div>
                    {/* <!-- end container-fluid --> */}
                </div>
                </div>
            }
            {/* <!-- end wrapper --><!-- Footer --> */}
                <Footer />
            
            </div>

            
        )
    }

}

export default AccuracyReport;