import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import Header from '../navigation/Header';
import { UploadFile } from '../services/PostData';
import { LoginData } from '../services/PostData';
import axios, { post }from 'axios'
class Upload extends Component {
    constructor() {
        super();
        this.state = {
            listFiles : [],
            listFile : 'false',
            login: true,
            clientList : [],
            to : '',
            from : ''
        };
        this.onFormSubmit = this.onFormSubmit.bind(this)
        this.onChange = this.onChange.bind(this)
        this.fileUpload = this.fileUpload.bind(this)
        this.handleList = this.handleList.bind(this)
        
    }
    handleList() {
        // console.log(JSON.parse(sessionStorage.getItem('userData')).UserID);
        axios.post('http://localhost/regex/getUpload.php?userId=' + JSON.parse(sessionStorage.getItem('userData')).UserID ).then(response => {
            console.log(response)
            this.setState({
                listFile : 'true',
                listFiles: response.data.result
            })
        }).catch(error => {
            console.log(error);
        });

    }

    componentWillMount(){
        axios.get('http://localhost/regex/clientlist.php' ).then(response => {
            this.setState({
                clientList : response.data.result
            });
        }).catch(error => {
            console.log(error);
        });
        this.handleList();
    }
    onFormSubmit(e) {
        e.preventDefault() // Stop form submit
        this.fileUpload(this.state.file);
    }
    onChange(e) {
        this.setState({ file: e.target.files[0] })
    }
    fileUpload(file) {
        var formData = new FormData();
        formData.append("file", file);
        console.log(sessionStorage.getItem('userData'))
        formData.append("username", JSON.parse(sessionStorage.getItem('userData')).UserName);
        formData.append("userid", JSON.parse(sessionStorage.getItem('userData')).UserID);
        formData.append("from",this.state.from );
        formData.append("to", this.state.to);
        if(this.state.from != '' && this.state.to != ''){
            if(this.state.from ===  this.state.to ){
                alert('Please Select different CLients from Dropdown.')
                return false;
            }
            axios.post('http://localhost/regex/upload.php', formData, {
            headers: { 'Content-Type': 'multipart/form-data;text/html' }
            }).then(response => {
                this.handleList();
                alert('File Uploaded')
            }).catch(error => {
                console.log(error);
            });    
        }else{
            alert('Please Select Clients From  the Dropdown')
        }
        
        
    }
    
    handleLogout = () => {
        sessionStorage.removeItem('userData');
        this.setState({
            login: false
        })
    }

    handleUserInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        var alldata = this.state;
        alldata[name] = value
        this.setState(alldata);
    }


    
    render(){
        console.log(this.state);
        const values = Array.from(this.state.listFiles).map((number, index) =>
             <div key={index}>{number}</div>
        );
        const searchResults = this.state.listFiles.map((data,key) =>
            <tr>
                <td>{data.FileID}</td>
                <td>{data.FileTypeID}</td>
                <td>{data.ReportTypeID}</td>
                <td>{data.UserID}</td>
                <td>{data.FromClientID}</td>
                <td>{data.ToClientID}</td>
                <td>{data.FileName}</td>
                <td>{data.ImportDateTime}</td>
                <td>{data.ProcessDateTime}</td>
                <td>{data.ErrorDescription}</td>
                <td>{data.FileStatus}</td>
                <td>{data.CreateTimestamp}</td>
                <td>{data.CreateUser}</td>
                <td>{data.ModifiedTimestamp}</td>
                <td>{data.ModifiedUser}</td>
                <td>{data.IsActive}</td>
                <td>{data.STATUS == 0 ? ( <button className="btn btn-defualt">Reconunceling</button> ) : data.STATUS}</td>
            </tr>
        )
        if (!sessionStorage.getItem('userData')) {
            return <Redirect exact push to={{ pathname: '/login' }} />
        }
        if (!this.state.login) {
            //return <Redirect exact push to={{ pathname: '/login' }} />
        }

        var allProfiles = this.state.clientList.map(function(data,number){
            return <option key={number} value={data.ClientID}>{data.ClientName}</option>     
        });
        return (
            <div >
                <button onClick={() => this.handleLogout()} className="btn btn-primary">Logout</button>
                <div>
                    <h2>Upload File</h2>
                    <div className="container">
                        <div className="demoForm">
                            <form onSubmit={this.onFormSubmit}>
                                <div className={`form-group col-md-12 `}>
                                    <label className="col-md-6" htmlFor="email">File Upload</label>
                                    <input type="file" onChange={this.onChange} className=" form-control" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="state" className="col-md-2 control-label">From </label>
                                    <div className="col-md-6">
                                        <select className="form-control" defaultValue='' onChange={(event) => this.handleUserInput(event)} required="" id="from" name="from">
                                        <option value=''>Select</option>
                                            {allProfiles}
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="state" className="col-md-2 control-label">To </label>
                                    <div className="col-md-6">
                                        <select className="form-control" defaultValue='' onChange={(event) => this.handleUserInput(event)} required="" id="to" name="to">
                                        <option value=''>Select</option>
                                            {allProfiles}
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-9">
                                    <button type="submit" className="btn btn-primary ">Upload</button>
                                </div>
                            </form>
                            {/* <div className="cols col-md-3">
                        <a href="/forgot-password" className="btn btn-primary">Forgot Password</a>
                    </div> */}

                        </div>
                        
                            
                        
                    </div>
                </div>
                <div className="" style={{ marginTop : '110px'}}>
                    {/*<label>Sync files <button onClick={this.handleList} className="btn btn-primary">Sync</button></label>*/}
                    <div>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>FileTypeID</th>
                                    <th>ReportTypeID</th>
                                    <th>UserID</th>
                                    <th>FromClientID</th>
                                    <th>ToClientID</th>
                                    <th>FileName</th>
                                    <th>ImportDateTime</th>
                                    <th>ProcessDateTime</th>
                                    <th>ErrorDescription</th>
                                    <th>FileStatus</th>
                                    <th>CreateTimestamp</th>
                                    <th>CreateUser</th>
                                    <th>ModifiedTimestamp</th>
                                    <th>ModifiedUser</th>
                                    <th>IsActive</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                {searchResults}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            
        )
    }

}

export default Upload;