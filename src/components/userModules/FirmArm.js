import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Header from '../navigation/Header';
import Footer from '../navigation/Footer';
import Filter from '../navigation/Filter';
// import { Line, Bar } from 'react-chartjs-2';
import axios, { post } from 'axios'
import api from '../api.js'
import moment from 'moment';
import Helmet from 'react-helmet';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

import { formatDate, parseDate } from 'react-day-picker/moment';
let ckdArr = [];
const ckdArrForArm = [];
const plainOptionsFrm = [];
const plainOptionsArm = [];
class FirmArm extends Component {
    constructor() {
        super();
        var tempDate = new Date();
        var firstDay = new Date(tempDate.getFullYear(), tempDate.getMonth(), 1);
        //var firstDay = new Date(tempDate.getFullYear(), 10, 1);
        var tempDate = new Date();
        var date = tempDate.getFullYear() + '-' + (tempDate.getMonth() + 1) + '-' + tempDate.getDate();
        
        this.state = {
            recouncilationdata: [],
            recouncilationColumns: [],
            armrecouncilationdata: [],
            armrecouncilationColumns: [],
            userData: JSON.parse(sessionStorage.getItem('userData')),
            loading: false,
            ARMReportedLoading: false,
            date: date,
            from: firstDay,
            to: new Date(),
            breakStatus:'0',
            transpose: '',
            refresh:'',
            alltrancation: '',
            id: [],
            idForArm: [],
            alltrancation: '',
            singleCheckbox: [],
            checkedListArm: [],
            checkedListFrm: [],
            checkAll: false    
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleFromChange = this.handleFromChange.bind(this);
        this.handleToChange = this.handleToChange.bind(this);
        this.handleTransaction = this.handleTransaction.bind(this);
        this.handleTrans = this.handleTrans.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
        this.handleSingleCheckBox = this.handleSingleCheckBox.bind(this);
        this.handleSingleCheckBoxForArm = this.handleSingleCheckBoxForArm.bind(this);
       
        
    }
    
    handelList = (breakStatus) => {
        
        this.setState({ ARMReportedLoading: false });
        var from = new Date(this.state.from);
        //from = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        from = from.getFullYear() + '-' + 10 + '-' + from.getDate();
        var to = new Date(this.state.to);
        to = to.getFullYear() + '-' + (to.getMonth() + 1) + '-' + to.getDate();
        //to = to.getFullYear() + '-' + 10 + '-' + to.getDate();
        
        let url = api.url + api.getRecouncilation + '?api_token=' + this.state.userData.API_Token + '&clientName=' + this.state.userData.ClientName + '&ReconciliationType=firm-arm&RecAttribute=RecordMatching&FileFromDate=' + from + '&FileToDate=' + to + '&breackStatus=' + breakStatus;
       
       
        axios.get(url).then(response => {
            if (response.data.status) {
                this.setState({
                    recouncilationdata: response.data.result.recouncilation,
                    recouncilationColumns: response.data.result.columns,
                });
            } else {
                this.setState({
                    recouncilationdata: [],
                    recouncilationColumns: [],
                });
               // alert(response.data.message)
            }
        }).catch(error => {
            console.log(error);
        });
        this.setState({ ARMReportedLoading: true });
    }

    handelListForArm = (breakStatus) => {
        this.setState({ ARMReportedLoading: false });
        var from = new Date(this.state.from);
        from = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        var to = new Date(this.state.to);
        to = to.getFullYear() + '-' + (to.getMonth() + 1) + '-' + to.getDate();
        let url = api.url + api.getRecouncilation + '?api_token=' + this.state.userData.API_Token + '&clientName=' + this.state.userData.ClientName + '&ReconciliationType=arm-firm&RecAttribute=RecordMatching&FileFromDate=' + from + '&FileToDate=' + to + '&breackStatus=' + breakStatus; 
        axios.get(url).then(response => {
            if (response.data.status) {
                this.setState({
                    armrecouncilationdata: response.data.result.recouncilation,
                    armrecouncilationColumns: response.data.result.columns,
                });
            } else {
                this.setState({
                    armrecouncilationdata:[],
                    armrecouncilationColumns: [],
                });
               // alert(response.data.message)
            }
        }).catch(error => {
            console.log(error);
        });
        this.setState({ ARMReportedLoading: true });
    }

    handleChange(date) {
        var tempDate = new Date(date);
        var date = tempDate.getFullYear() + '-' + (tempDate.getMonth() + 1) + '-' + tempDate.getDate();
        
        this.setState({
            date: date
        });
       
        this.handelList(0);
        this.handelListForArm(0);

    }

    componentWillMount() {

        this.setState({ loading: false });
        // this.setState({breakStatus:'0'});
        this.handelList(0);
        this.handelListForArm(0)
        this.setState({ loading: true });
    }
    
    columnsRecouncilation(columnsRecouncilationmap) {
        let columns = [];
        columns.push(
            
        )
        Object.keys(columnsRecouncilationmap).map(function (data) {
            const valueckd = data.toString();
            columns.push(<td key={valueckd}>{columnsRecouncilationmap[data]}</td>)
        });
        return columns;
    }

    // generateRows = (recouncilationdata) => {
    //     const rows = [];
    //     for (let i = 0; i < recouncilationdata.length; i++) {
    //         rows.push(
    //             <tr key={`row_${i}`} >
    //                 <td><div className="checkbox-wrapper"><input  id={`column_${i}`} type="checkbox" onChange={this.handleCheckChieldElement} name="checkedAll" checked={this.state.isChecked}  /> <label htmlFor="chk20" className="toggle"></label></div></td>
    //                 {this.columnsRecouncilation(recouncilationdata[i])}
    //             </tr>
    //         );
    //     }

    //     return rows;
    // }

    // generateRowsArm = (recouncilationdata) => {
    //     const rows = [];
    //     for (let i = 0; i < recouncilationdata.length; i++) {
    //         rows.push(
    //             <tr key={`row_${i}`} >
    //                <td><div className="checkbox-wrapper"><input  id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
    //                 {this.columnsRecouncilation(recouncilationdata[i])}
    //             </tr>
    //         );
    //     }

    //     return rows;
    // }
    showFromMonth() {
        const { from, to } = this.state;
        if (!from) {
            return;
        }
        if (moment(to).diff(moment(from), 'months') < 2) {
            this.to.getDayPicker().showMonth(from);
        }
    }
    handleFromChange(from) {
        // Change the from date and focus the "to" input field
        this.setState({ from });
        //this.handelList()
    }
    handleToChange(to) {
        this.setState({ to }, this.showFromMonth);
        this.handelList(0);
        this.handelListForArm(0);

    }

    getanotherColumnTranspose(column, status) {
        let columnName = column;
        let columns = [];
        var columnsRecouncilationmap = this.state.recouncilationdata
        if (status) {
            for (let i = 0; i < columnsRecouncilationmap.length; i++) {
                columns.push(<td  key={`col_${i}`}>{columnsRecouncilationmap[i][columnName]}</td>)
            }
        } else {
            for (let i = 0; i < columnsRecouncilationmap.length; i++) {
                columns.push(<td  key={`row_${i}`}><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>)
            }
        }
        return columns;
    }

    generateRowsWhenTranspose() {

        if (this.state.transpose) {
            let columns = this.state.recouncilationColumns;
            var columnsRecouncilationmap = this.state.recouncilationdata.length;
            if (!(columns.length == 0)) {
                const rows = [];
                rows.push(
                  
                  <tr key={`row_`}>
                        <th ><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label htmlFor="chk20" className="toggle"></label></div></th>
                        {this.getanotherColumnTranspose(0, false)}
                    </tr>
                  
                )
                for (let i = 0; i < columns.length; i++) {
                    const value = columns[i].split('_').join(' ');
                    rows.push(
                       
                        <tr key={`row_${i}`} >
                            <th>{value}</th>
                            {this.getanotherColumnTranspose(columns[i], true)}
                        </tr>
                        
                    );
                }

                return rows;
            }
        }
    }

    getanotherColumnTransposeForArm(column, status) {
        let columnName = column;
        let columns = [];
        var columnsRecouncilationmap = this.state.armrecouncilationdata
        if (status) {
            for (let i = 0; i < columnsRecouncilationmap.length; i++) {
                columns.push(<td>{columnsRecouncilationmap[i][columnName]}</td>)
            }
        } else {
            for (let i = 0; i < columnsRecouncilationmap.length; i++) {
                columns.push(<td><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>)
            }
        }
        return columns;
    }


    generateRowsWhenTransposeForArm() {
        if (this.state.transpose) {
            let columns = this.state.armrecouncilationColumns;
            var columnsRecouncilationmap = this.state.armrecouncilationdata.length;
            if (!(columns.length == 0)) {
                const rows = [];
                rows.push(
                    
                    <tr key={`row_`}>
                        <th><div className="checkbox-wrapper"><input id="chk20" type="checkbox"  /> <label htmlFor="chk20" className="toggle"></label></div></th>
                        {this.getanotherColumnTransposeForArm(0, false)}
                    </tr>
                    
                )
                for (let i = 0; i < columns.length; i++) {
                    const value = columns[i].split('_').join(' ');
                    rows.push(
                        
                        <tr key={`row_${i}`} >
                            <th>{value}</th>
                            {this.getanotherColumnTransposeForArm(columns[i], true)}
                        </tr>
                        
                    );
                }
                return rows;
            }
        }
    }

     generateRows = (recouncilationdata) => {
        const rows = [];
        for (let i = 0; i < recouncilationdata.length; i++) {
            const checked = false;
            if (this.state.id.indexOf(`column_${i}`) >= 0 || this.state.isAllSelectedFrm) {
                checked = true;
            }
            plainOptionsFrm[i] = (`column_${i}`);
            
            rows.push(
                <tr key={`row_${i}`} >
                    <td><div className="checkbox-wrapper"><input id={`column_${i}`} checked={checked} name="checkedAll" onChange={this.handleSingleCheckBox} type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                    {this.columnsRecouncilation(recouncilationdata[i])}
                </tr>
            );
        }
        return rows;
    }
    armcolumnsRecouncilation(armcolumnsRecouncilationmap) {
        let columns = [];
        columns.push(

        )
        Object.keys(armcolumnsRecouncilationmap).map(function (data) {
            const valueckd = data.toString();
            columns.push(<td key={valueckd}>{armcolumnsRecouncilationmap[data]}</td>)
        });
        return columns;
    }

    generateRowsArm = (armrecouncilationdata) => {
        const rows = [];
        
        for (let i = 0; i < armrecouncilationdata.length; i++) {
           
            var checked = false;
            if (this.state.idForArm.indexOf(`column_${i}`) >= 0 || this.state.isAllSelectedArm) {
                checked = true;
            }
            plainOptionsArm[i] = (`column_${i}`);
            
            
            rows.push(
                <tr key={`row_${i}`} >
                    <td><div className="checkbox-wrapper"><input id={`column_${i}`} type="checkbox" checked={checked}  onChange={this.handleSingleCheckBoxForArm} onClick={this.handleSingleCheckBoxArm} /> <label htmlFor="chk20" className="toggle"></label></div></td>
                    {this.armcolumnsRecouncilation(armrecouncilationdata[i])}
                </tr>
            );
        }
        
        return rows;
    }

    handleSingleCheckBoxForArm(e) {
        var ckAdrrNew = [];
        ckAdrrNew = this.state.idForArm;//this.state.id;
        if (this.state.isAllSelectedArm) {
            ckAdrrNew = plainOptionsArm;
            const parts = e.target.id.split("_");
            var ckAdrrNewChecked = ckAdrrNew.filter(function (ele) {
                if (ele != e.target.id) {
                    return ele;
                }
            });
        } else {
            if (e.target.checked) {
                const parts = e.target.id.split("_");
                ckAdrrNew[parts[1]] = e.target.id;
                var ckAdrrNewChecked = ckAdrrNew

            } else {
                const parts = e.target.id.split("_");
                var ckAdrrNewChecked = ckAdrrNew.filter(function (ele) {
                    if (ele != e.target.id) {
                        return ele;
                    }
                });
            }
        }
        ckdArrForArm = ckAdrrNewChecked;
        const uniqueNames = Array.from(new Set(ckdArrForArm));
        this.setState({ idForArm: uniqueNames, isAllSelectedArm: false });
    }


   


    handleSingleCheckBox(e){
        var ckAdrrNew = [];
        ckAdrrNew = this.state.id;
        
        if (this.state.isAllSelectedFrm){
            ckAdrrNew = plainOptionsFrm; 
            var ckAdrrNewChecked = ckAdrrNew.filter(function(ele){
                if (ele != e.target.id){
                    return ele;
                }
            });
        }else{
        
            if(e.target.checked){
                const parts = e.target.id.split("_");
                ckAdrrNew[parts[1]] = e.target.id;
                var ckAdrrNewChecked = ckAdrrNew
            } else {
                const parts = e.target.id.split("_");
                var ckAdrrNewChecked = ckAdrrNew.filter(function (ele) {
                    if (ele != e.target.id) {
                        return ele;
                    }
                });
            }
        }
        ckdArr = ckAdrrNewChecked;
        const uniqueNames = Array.from(new Set(ckdArr));
        this.setState({ id: uniqueNames, isAllSelectedFrm:false});
    }

    handleTrans(transValue){
        this.setState({ transpose: transValue });
        
    }
    handleRefresh(RefreshValue){
        this.setState({refresh:RefreshValue});
        this.handelList(0);
        this.handelListForArm(0);
    }
    handleTransaction(transactionValue){
        this.handelList('');
        this.handelListForArm('');
    }

    onCheckBoxChange(checkName, isChecked,type) {
        const checked = isChecked;
        if (type == 'frm'){
            this.setState({
                isAllSelectedFrm: checked,
                id: (checked ? plainOptionsFrm : [])
            });
        
        }
        if (type == 'arm') {
            this.setState({
                isAllSelectedArm: checked,
                idForArm: (checked ? plainOptionsArm : [])
            });

        }

        
    }
    
    render() {
        
        let chartData = {
            labels: ['A', 'B', 'C', 'D'],
            datasets: [
                {
                    backgroundColor: ["#dbdee7", "#dbdee7", "#00d563", "#dbdee7"],
                    borderColor: ["#dbdee7", "#dbdee7", "#00d563", "#dbdee7"],
                    borderWidth: 1,
                    data: [1, 10, 12, 15],
                },

            ],
        };
        var tableHeadwithColumns;
        let columns = this.state.recouncilationColumns;
        if (!(columns.length == 0)) {
            tableHeadwithColumns = columns.map(function (data) {
                
                const value = data.split('_').join(' ');
                return <th key={value.toString()}>{value}</th>
            });
        }
        var tableHeadwithColumnsArm;
        let columnsarm = this.state.armrecouncilationColumns;
        if (!(columnsarm.length === 0)) {
            tableHeadwithColumnsArm = columnsarm.map(function (data) {
                
                const value = data.split('_').join(' ');
                return <th key={value.toString()}>{value}</th>
            });
        }
        const { from, to } = this.state;
        const modifiers = { start: from, end: to };
        return (

            <div >
                {!this.state.loading ? <div><i className="fa fa-spin fa-fw fa-spinner"></i> Loding</div> :
                    <div>
                        <Header activeClass="firmArm" />
                        <div className="wrapper">
                            <div className="container-fluid">
                                {/* <!-- Page-Title --> */}
                                <div className="row">
                                    <div className="col-xl-6 col-sm-6">
                                        <div className="page-title-box">
                                            <h4 className="page-title">MiFD II - Completeness > Firm-ARM Reconciliation</h4>
                                        </div>
                                    </div>
                                    <Filter ontransClick={this.handleTrans}
                                        onTransactionclick={this.handleTransaction}
                                        onRefreshClick={this.handleRefresh}
                                        inputVal={this.state.id}
                                        datarow={this.state.recouncilationdata}
                                        datacol={this.state.recouncilationColumns}
                                        inputValForArm={this.state.idForArm}
                                        datarowForArm={this.state.armrecouncilationdata}
                                        datacolForArm={this.state.armrecouncilationColumns}
                                        isAllselectForFirm={this.state.isAllSelectedFrm}
                                        isAllselectForArm={this.state.isAllSelectedArm}
                                        //datacolForArm={this.state.plainOptionsFrm}


                                    />
                                </div>
                            </div>
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-xl-12">
                                        <div className="card m-b-20">
                                            <div className="card-body">
                                                <h4 className="mt-0 m-b-10 header-title float-left">Firm Transaction</h4>
                                                <div className="InputFromTo">
                                                    <DayPickerInput
                                                        value={from}
                                                        className="date-input"
                                                        placeholder="From"
                                                        format="LL"
                                                        formatDate={formatDate}
                                                        parseDate={parseDate}
                                                        dayPickerProps={{
                                                            selectedDays: [from, { from, to }],
                                                            disabledDays: { after: to },
                                                            toMonth: to,
                                                            modifiers,
                                                            numberOfMonths: 2,
                                                            onDayClick: () => this.to.getInput().focus(),
                                                        }}
                                                        onDayChange={this.handleFromChange}
                                                    />
                                                    <span className="InputFromTo-to">
                                                        <DayPickerInput
                                                            ref={el => (this.to = el)}
                                                            value={to}
                                                            className="date-input"
                                                            placeholder="To"
                                                            format="LL"
                                                            formatDate={formatDate}
                                                            parseDate={parseDate}
                                                            dayPickerProps={{
                                                                selectedDays: [from, { from, to }],
                                                                disabledDays: { before: from },
                                                                modifiers,
                                                                month: from,
                                                                fromMonth: from,
                                                                numberOfMonths: 2,
                                                            }}
                                                            onDayChange={this.handleToChange}
                                                        />
                                                    </span>
                                                    <Helmet>
                                                        <style>{`

                                        .InputFromTo { margin:0 0 10px 25%; float:left; padding:0; text-align:center;  display:flex;}
                                        .DayPickerInput-OverlayWrapper { font-size:12px; left:0; right:auto;}
                                        .InputFromTo .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
                                            background-color: #f0f8ff !important;
                                            color: #4a90e2; font-size:12px;
                                        }
                                        .InputFromTo .DayPicker-Day {
                                            border-radius: 0 !important; font-size:12px;
                                        }
                                        .InputFromTo .DayPicker-Day--start {
                                            border-top-left-radius: 50% !important; font-size:12px;
                                            border-bottom-left-radius: 50% !important;
                                        }
                                        .InputFromTo .DayPicker-Day--end {
                                            border-top-right-radius: 50% !important; font-size:12px;
                                            border-bottom-right-radius: 50% !important;
                                        }
                                        .InputFromTo .DayPickerInput-Overlay {
                                            width: 520px; font-size:12px;
                                        }
                                        .InputFromTo-to .DayPickerInput-Overlay {
                                            margin-left: 0;
                                        }
                                        `}</style>
                                                    </Helmet>
                                                </div>
                                                {!this.state.ARMReportedLoading ? <div><i className="fa fa-spin fa-fw fa-spinner"></i> Loding</div> :

                                                    <div>
                                                        {this.state.transpose ?
                                                            <div className="table-responsive">
                                                                <table className="fixed_header table table-striped table-bordered table-hover">
                                                                    <tbody> {this.generateRowsWhenTranspose()}</tbody>
                                                                </table>
                                                            </div>
                                                            :
                                                            <div className="table-responsive">
                                                                <table className="fixed_header table table-striped table-bordered table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>
                                                                                <div className="checkbox-wrapper">
                                                                                    <input id="chk20" checked={this.state.isAllSelectedFrm} onChange={(e) => this.onCheckBoxChange('all',e.target.checked,'frm')} type="checkbox" />
                                                                                 <label htmlFor="chk20" className="toggle"></label>
                                                                                </div>
                                                                            </th>
                                                                            {tableHeadwithColumns}
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        {this.generateRows(this.state.recouncilationdata)}
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        }
                                                    </div>
                                                }
                                                <h4 className="mt-0 m-b-10 header-title float-left">Arm Transaction</h4>
                                                <div>
                                                    {this.state.transpose ?
                                                        <div className="table-responsive">
                                                            <table className="fixed_header table table-striped table-bordered table-hover">
                                                                <tbody>
                                                                    {this.generateRowsWhenTransposeForArm()}
                                                                </tbody>
                                                                
                                                            </table>
                                                        </div>
                                                        :
                                                        <div className="table-responsive">
                                                            <table className="fixed_header table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <div className="checkbox-wrapper">
                                                                                <input id="chk20" checked={this.state.isAllSelectedArm} onChange={(e) => this.onCheckBoxChange('all', e.target.checked, 'arm')} type="checkbox"/> <label htmlFor="chk20" className="toggle"></label>
                                                                            </div>
                                                                        </th>
                                                                        {tableHeadwithColumnsArm}
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    {this.generateRowsArm(this.state.armrecouncilationdata)}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                            {/* <!-- end of table responsive --> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* <!-- end row --> */}

                            {/* <!-- end container-fluid --> */}
                        </div>
                        {/* <!-- end wrapper --><!-- Footer --> */}
                        <Footer />
                    </div>
                }
            </div>



        )
    }

}

export default FirmArm;