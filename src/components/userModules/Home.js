import React, { Component } from 'react'; 
import { Redirect } from 'react-router-dom';
import Header from '../navigation/Header';
import { CustomerData } from '../services/PostData';

class Home extends Component {
    constructor() {
        super();
        this.state = {
            login : true,
            userdetails: JSON.parse(sessionStorage.getItem('userData'))
        };

    }
    handleLogout = () => {
        sessionStorage.removeItem('userData');
        this.setState({
            login : false
        })
    }
    render() {
        if (!sessionStorage.getItem('userData')) {
            return <Redirect exact push to={{ pathname: '/login' }} />
        }
        if (!this.state.login){
            return <Redirect exact push to={{ pathname: '/login' }} />
        }
        
        return (
            
            <div className="customerList">
                <button onClick={() => this.handleLogout()} className="btn btn-primary">Logout</button>
                {/* <a href="/upload" className="side-link">Upload File</a> */}
                <Header activeClass="accuracyInstrument" />
                <h2>User Profile</h2>

                <div className="col-md-12"><label><span>ClientID</span><p>{this.state.userdetails.ClientID}</p></label></div>
                <div className="col-md-12"><label><span>CreateTimestamp</span><p>{this.state.userdetails.CreateTimestamp}</p></label></div>
                <div className="col-md-12"><label><span>CreateUser</span><p>{this.state.userdetails.CreateUser}</p></label></div>
                <div className="col-md-12"><label><span>ModifiedUser</span><p>{this.state.userdetails.ModifiedUser}</p></label></div>
                <div className="col-md-12"><label><span>UserName</span><p>{this.state.userdetails.UserName}</p></label></div>
                
            </div>
            
        );
    }
};
export default Home;