import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Header from '../navigation/Header';
import Footer from '../navigation/Footer';
// import { Line, Bar } from 'react-chartjs-2';
import axios, { post } from 'axios'
import api from '../api.js'
import DatePicker from "react-datepicker";
import moment from 'moment';
import Helmet from 'react-helmet';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

import { formatDate, parseDate } from 'react-day-picker/moment';


class DataImport extends Component {
    constructor() {
        super();

        var tempDate = new Date();
        var firstDay = new Date(tempDate.getFullYear(), tempDate.getMonth(), 1);
        this.state = {
            getData: [],
            userData: JSON.parse(sessionStorage.getItem('userData')),
            loading: false,
            from: firstDay,
            to: new Date(),
            dataLoading: false,
            
        };
  
        this.handleFromChange = this.handleFromChange.bind(this);
        this.handleToChange = this.handleToChange.bind(this);
    }

    handelList = () => {
        this.setState({ dataLoading: false });
        var from = new Date(this.state.from);
        from = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        var to = new Date(this.state.to);
        to = to.getFullYear() + '-' + (to.getMonth() + 1) + '-' + to.getDate();
        let url = api.url + api.dataImport + '?&clientId=' + this.state.userData.ClientName + '&from_date=' + from + '&to_date=' + to +'&api_token=' + this.state.userData.API_Token ;

        axios.get(url).then(response => {
            console.log(response)
            this.setState({
                dataLoading: true,
                getData : response.data.result
            });
        }).catch(error => {
            console.log(error);
        });

    }
    showFromMonth() {
        const { from, to } = this.state;
        if (!from) {
            return;
        }
        if (moment(to).diff(moment(from), 'months') < 2) {
            this.to.getDayPicker().showMonth(from);
        }
    }
    handleFromChange(from) {
        // Change the from date and focus the "to" input field
        this.setState({ from });
        //this.handelList()
    }
    handleToChange(to) {
        this.setState({ to }, this.showFromMonth);
        this.handelList()

    }
    componentWillMount() {

        this.setState({ loading: false });
        console.log(this.state)
        this.handelList()
        this.setState({ loading: true });
    }
    columnsgetData(getColumnData) {
        let columns = [];
        
        Object.keys(getColumnData).map(function (data, number) {
            columns.push(<td>{getColumnData[data]}</td>)
        });
        return columns;
    }

    generateRows = (getData) => {
        const rows = [];
        for (let i = 0; i < getData.length; i++) {
            rows.push(
                <tr key={`row_${i}`} >
                    {this.columnsgetData(getData[i])}
                </tr>
            );
        }

        return rows;
    }

    // handleChangeStart(date) {
    //     var tempDateCheck = new Date(date);
    //     var tempDate = tempDateCheck.getFullYear() + '-' + (tempDateCheck.getMonth() + 1) + '-' + tempDateCheck.getDate();
    //     console.log("Current Date= " + tempDateCheck);
    //     this.setState({
    //         fromdate: tempDateCheck,
    //         fromdateImport: tempDate
    //     });
    //     this.handelList()

    // }
    // handleChangeEnd(date) {
    //     var tempDateCheck = new Date(date);
    //     var tempDate = tempDateCheck.getFullYear() + '-' + (tempDateCheck.getMonth() + 1) + '-' + tempDateCheck.getDate();
    //     console.log("End Date= " + tempDateCheck);
    //     this.setState({
    //         enddate: tempDateCheck,
    //         enddateImport: tempDate
    //     });
    //     this.handelList()

    // }

    // showFromMonth() {
    //     const { fromdate, enddate } = this.state;
    //     if (!fromdate) {
    //         return;
    //     }
    //     if (moment(enddate).diff(moment(fromdate), 'months') < 2) {
    //         this.enddate.getDayPicker().showMonth(fromdate);
    //     }
    // }

    // handleToChange(enddate) {
    //     this.setState({ enddate }, this.showFromMonth);
    // }
    // handleFromChange(fromdate) {
    //     // Change the from date and focus the "to" input field
    //     this.setState({ fromdate });
    // }

    

    render() {
        // const { fromdate, enddate } = this.state;
        // const modifiers = { start: fromdate, end: enddate };
        const { from, to } = this.state;
        const modifiers = { start: from, end: to };
        return (
            <div >
                <Header activeClass="data-import" />
                <div className="wrapper">
                    <div className="container-fluid">
                        {/* <!-- Page-Title --> */}
                        <div className="row">
                            <div className="col-xl-6 col-sm-6">
                                <div className="page-title-box">
                                    <h4 className="page-title">Data Import</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-xl-12">
                                <div className="card m-b-20">
                                    <div className="card-body">
                                        <h4 className="mt-0 m-b-10 header-title float-left">Data Import Data</h4>

                                       
                                        <div className="InputFromTo">
                                            <DayPickerInput
                                                value={from}
                                                className="date-input"
                                                placeholder="From"
                                                format="LL"
                                                formatDate={formatDate}
                                                parseDate={parseDate}
                                                dayPickerProps={{
                                                    selectedDays: [from, { from, to }],
                                                    disabledDays: { after: to },
                                                    toMonth: to,
                                                    modifiers,
                                                    numberOfMonths: 2,
                                                    onDayClick: () => this.to.getInput().focus(),
                                                }}
                                                onDayChange={this.handleFromChange}
                                            />
                                            <span className="InputFromTo-to">
                                                <DayPickerInput
                                                    ref={el => (this.to = el)}
                                                    value={to}
                                                    className="date-input"
                                                    placeholder="To"
                                                    format="LL"
                                                    formatDate={formatDate}
                                                    parseDate={parseDate}
                                                    dayPickerProps={{
                                                        selectedDays: [from, { from, to }],
                                                        disabledDays: { before: from },
                                                        modifiers,
                                                        month: from,
                                                        fromMonth: from,
                                                        numberOfMonths: 2,
                                                    }}
                                                    onDayChange={this.handleToChange}
                                                />
                                            </span>
                                            <Helmet>
                                                <style>{`

                                                .InputFromTo { margin:0 0 10px 25%; float:left; padding:0; text-align:center;  display:flex;}
                                                .DayPickerInput-OverlayWrapper { font-size:12px; left:0; right:auto;}
                                                .InputFromTo .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
                                                    background-color: #f0f8ff !important;
                                                    color: #4a90e2; font-size:12px;
                                                }
                                                .InputFromTo .DayPicker-Day {
                                                    border-radius: 0 !important; font-size:12px;
                                                }
                                                .InputFromTo .DayPicker-Day--start {
                                                    border-top-left-radius: 50% !important; font-size:12px;
                                                    border-bottom-left-radius: 50% !important;
                                                }
                                                .InputFromTo .DayPicker-Day--end {
                                                    border-top-right-radius: 50% !important; font-size:12px;
                                                    border-bottom-right-radius: 50% !important;
                                                }
                                                .InputFromTo .DayPickerInput-Overlay {
                                                    width: 520px; font-size:12px;
                                                }
                                                .InputFromTo-to .DayPickerInput-Overlay {
                                                    margin-left: 0;
                                                }
                                                `}</style>
                                            </Helmet>
                                        </div>
                                                                            
                                        <div className="table-responsive">
                                            <table className="table table-striped table-bordered table-hover tbl_scroll dataimport-tbl ">
                                                <thead>
                                                    <tr>
                                                        <th width="30%">File Name</th>
                                                        <th width="15%">Source</th>
                                                        <th width="10%">File Type</th>
                                                        <th width="10%">No. of Rows</th>
                                                        <th width="10%">Processing Status</th>
                                                        <th width="15%">Reason Code</th>
                                                        <th width="10%">Uploaded by</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.generateRows(this.state.getData)}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* <!-- end wrapper --><!-- Footer --> */}
                <Footer />
            </div >


        )
    }

}

export default DataImport;