import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Header from '../navigation/Header';
import Footer from '../navigation/Footer';
// import { Line, Bar } from 'react-chartjs-2';
import axios, { post }from 'axios'
class AccuracyInstrument extends Component {
    constructor() {
        super();
        this.state = {
        };
    }
    


    
    render(){
        let chartData = {
            labels: ['A', 'B', 'C', 'D'],
            datasets: [
                {
                    backgroundColor: ["#dbdee7", "#dbdee7", "#00d563", "#dbdee7"],
                    borderColor: ["#dbdee7", "#dbdee7", "#00d563", "#dbdee7"],
                    borderWidth: 1,
                    data: [1, 10, 12, 15],
                },

            ],
        };
        return (
            <div >
                <Header activeClass="accuracyInstrument" />
                <div className="wrapper">
			        <div className="container-fluid">
					{/* <!-- Page-Title --> */}
					    <div className="row">
					        <div className="col-xl-6 col-sm-6">
                                <div className="page-title-box">
							        <h4 className="page-title">MiFD II - Accuracy > Reporting Eligibility</h4>
                                </div>
                            </div>
                            <div className="col-xl-6 col-sm-6 pull-right">
                                <div className="filter-search-div">
                                    <div className="filter-nav-div ">
                                        <div className="filt-nav-ul clearfix">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        <input type="file" id="file1" name="image" accept="image/*" capture style={{"display":"none"}}/><span id="upfile1" className="ion-upload" data-toggle="tooltip" data-placement="top" data-title="Upload" title="Upload"></span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <span className="ion-refresh" data-toggle="tooltip" data-placement="top" data-title="Refresh" title="Refresh"></span>
                                                    </a>
                                                </li>
                                                {/* <!--li><a href="#"><span className="ion-search" data-toggle="tooltip" data-placement="top" data-title="Search" title="Search"></span></a></li--> */}
                                                {/* <li><a href="#"><span className="ion-android-sort" data-toggle="tooltip" data-placement="top" data-title="Sort" title="Sort"></span></a></li> */}
                                                <li>
                                                    <a href="#">
                                                        <span className="ion-android-arrow-down-right" data-toggle="tooltip" data-placement="top" data-title="Transpose" title="Transpose"></span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <span className="ion-flash" data-toggle="tooltip" data-placement="top" data-title="Focus" title="Focus">
                                                        </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <span className="ion-magnet" data-toggle="tooltip" data-placement="top" data-title="Match" title="Match"></span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <span className="ion-social-buffer" data-toggle="tooltip" data-placement="top" data-title="All Transaction" title="All Transaction"></span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <span className="ion-compose" data-toggle="tooltip" data-placement="top" data-title="Edit" title="Edit"></span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    {/* <!-- end of filter-nav-div--> */}
                                    <div className="search-data-div">
                                        <div className="clearfix">
                                            <form>
                                                <div className="search-col-1"><input type="text" className="form-control" placeholder="Search"/></div>
                                                <div className="search-col-6"><button className="btn sbtn">Search</button></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                {/* <!-- end of filter-search-div --> */}
                            </div>
                        </div>
				    </div>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-xl-12">
                                <div className="card m-b-20">
                                    <div className="card-body">
                                        <h4 className="mt-0 m-b-10 header-title float-left">ARM Reported ISINs</h4>
                                        <div className="table-responsive">
                                            <table className="fixed_header table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <div className="checkbox-wrapper">
                                                                <input id="chk20" type="checkbox" /> <label for="chk20" className="toggle"></label>
                                                            </div>
                                                        </th>
                                                        <th>Date</th>
                                                        <th>Chk</th>
                                                        <th>Col 3</th>
                                                        <th>Col 4</th>
                                                        <th>Col 5</th>
                                                        <th>Col 6</th>
                                                        <th>Col 7</th>
                                                        <th>Col 8</th>
                                                        <th>Col 9</th>
                                                        <th>Col 10</th>
                                                        <th>Col 11</th>
                                                        <th>Col 12</th>
                                                        <th>Col 13</th>
                                                        <th>Col 14</th>
                                                        <th>Col 15</th>
                                                        <th>Col 16</th>
                                                        <th>Col 17</th>
                                                        <th>Col 18</th>
                                                        <th>Col 19</th>
                                                        <th>Col 20</th>
                                                        <th>Col 21</th>
                                                        <th>Col 22</th>
                                                        <th>Col 23</th>
                                                        <th>Col 24</th>
                                                        <th>Col 25</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox" /> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>

                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>

                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>

                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>

                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>

                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>                        
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>

                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>     <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <h4 className="mt-0 m-b-10 header-title float-left">FIRDS ISINs</h4>
                                        <div className="table-responsive">
                                            <table className="fixed_header table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></th>
                                                        <th>Date</th>
                                                        <th>Chk</th>
                                                        <th>Col 3</th>
                                                        <th>Col 4</th>
                                                        <th>Col 5</th>
                                                        <th>Col 6</th>
                                                        <th>Col 7</th>
                                                        <th>Col 8</th>
                                                        <th>Col 9</th>
                                                        <th>Col 10</th>
                                                        <th>Col 11</th>
                                                        <th>Col 12</th>
                                                        <th>Col 13</th>
                                                        <th>Col 14</th>
                                                        <th>Col 15</th>
                                                        <th>Col 16</th>
                                                        <th>Col 17</th>
                                                        <th>Col 18</th>
                                                        <th>Col 19</th>
                                                        <th>Col 20</th>
                                                        <th>Col 21</th>
                                                        <th>Col 22</th>
                                                        <th>Col 23</th>
                                                        <th>Col 24</th>
                                                        <th>Col 25</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>     <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div className="checkbox-wrapper"><input id="chk20" type="checkbox"/> <label for="chk20" className="toggle"></label></div></td>                        
                                                        <td>10/31/2018</td>
                                                        <td>4566</td>
                                                        <td>Col data 3</td>
                                                        <td>Col data 4</td>
                                                        <td>Col data 5</td>
                                                        <td>Col data 6</td>
                                                        <td>Col data 7</td>
                                                        <td>Col data 8</td>
                                                        <td>Col data 9</td>
                                                        <td>Col data 10</td>
                                                        <td>Col data 11</td>
                                                        <td>Col data 12</td>
                                                        <td>Col data 13</td>
                                                        <td>Col data 14</td>
                                                        <td>Col data 15</td>
                                                        <td>Col data 16</td>
                                                        <td>Col data 17</td>
                                
                                                        <td>Col data 18</td>
                                                        <td>Col data 19</td>
                                                        <td>Col data 20</td>
                                                        <td>Col data 21</td>
                                                        <td>Col data 22</td>
                                                        <td>Col data 23</td>
                                                        <td>Col data 24</td>
                                                        <td>Col data 25</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        {/* <!-- end of table responsive --> */}
                                    </div>
                                </div>
                            </div>
                        </div>               
            {/* <!-- end row --> */}
                    </div>
                    {/* <!-- end container-fluid --> */}
                </div>
            {/* <!-- end wrapper --><!-- Footer --> */}
                <Footer />
            </div>

            
        )
    }

}

export default AccuracyInstrument;