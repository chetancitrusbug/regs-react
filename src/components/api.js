export default {
    posts: '/posts',
    url: 'http://ec2-35-178-211-241.eu-west-2.compute.amazonaws.com/webservices/public/api/v1.0/',
    // localLoginUrl: 'http://ec2-35-177-183-16.eu-west-2.compute.amazonaws.com/webservices/public/api/v1.0/user/login/',
    // localuploadUrl: 'http://127.0.0.1/regex/upload.php',
    login:'user/login',
    getClient:'getclient',
    getClientType:'getclient-type',
    dataImport:'get-dataimport',
    uploadFiles: 'uploadfiles',
    getRecouncilation: 'get-recouncilation',

}