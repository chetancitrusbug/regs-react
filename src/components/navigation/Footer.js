import React, { Component } from 'react';

class Footer extends Component {

    state = {
        isOpen: false,
    };

    closeNavbar = () => {
        this.setState({ isOpen: false });
    }
    openNavBar = () => {
        this.setState({ isOpen: true });
    }
    render() {
        let openNav = this.state.isOpen;
        let classes = ['sidenav'];
        if (!!openNav) {
            classes.push('nav-open');
        }

        return (
            <div>
                <footer className="footer">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">© 2018 Regx</div>
                        </div>
                    </div>
                </footer>
            </div>

        )
    }
};


export default Footer;