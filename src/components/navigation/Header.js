import React, { Component } from 'react'; 
import { Redirect } from 'react-router-dom';
// import logo from '../static/images/logo.png';
import Logo from '../images/logo.png';
import Logosm from '../images/logo-sm.png';
import DummyUser from '../images/users/user-4.jpg';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            isNotification:false,
            active: this.props.activeClass
        };
    }
    handleLogout = () => {
        sessionStorage.removeItem('userData');
    }
    openNavBar = () => {
        this.setState({ isOpen: !this.state.isOpen });
    }
    openNotificationBar = () => {
        this.setState({ isNotification: !this.state.isNotification });
    }
    render() {
        
        if (!sessionStorage.getItem('userData')) {
            return <Redirect exact push to={{ pathname: '/' }} />
        }
    return (
        <div>
            {/* <div id="mySidenav" className={classes.join(' ')}>
                <a href="javascript:void(0)" className="closebtn" onClick={this.closeNavbar} >&times;</a>

                <div className="nav-logo">
                    <img src={logo} alt="" className="img-fluid" />
                </div>

                <div className="nav-user-div">
                    <span>Welcome <strong>User Name</strong></span>
                </div>

                <div className="side-links-div clearfix">
                    {/* <a href="#" className="side-link">Forgot Password</a> *}
                    <a href="/change-password" className="side-link">Change Password</a>
                    {/* <a href="/profile" className="side-link">Update Profile</a> *}
                    <a href="/" className="side-link">Customer Listing</a>
                    {/* <a href="/add-offer" className="side-link">Add Offer</a> *}
                    <a href="#" className="side-link">Offer Listing</a>
                    <a href="#" className="side-link">Job Listing</a>
                    <a href="#" className="side-link">Add Job</a>
                    <a href="#" className="side-link">Logout</a>
                </div>
            </div>
            <span className="nav-btn"><span onClick={this.openNavBar} className="menu-bar"></span></span> */}

            <header id="topnav">
                <div className="topbar-main">
                    <div className="container-fluid">
                      {/* <!-- Logo container--> */}
                        <div className="logo-div">
                            <a href="/home" className="logo">
                                <img src={Logosm} alt="" className="logo-small"/> 
                                <img src={Logo} alt="" className="logo-large"/>
                            </a>
                        </div>
                      {/* <!-- End Logo container--> */}
       
                        <div className="main-nav clearfix">
                            <ul>
                                <li><a href="#" className="active">MiFD II</a></li>
                                <li><a href="#">EMIR</a></li>
                                <li><a href="#">Dodd Frank</a></li>
                                <li><a href="#">Setup</a></li>
                            </ul>
                        </div>
       
                        <div className="menu-extras topbar-custom">
                            <ul className="float-right list-unstyled mb-0">
                                                
                                <li className="dropdown notification-list d-none d-sm-block">
                                    <form role="search" className="app-search">
                                        <div className="form-group mb-0">
                                            <input type="text" className="form-control" placeholder="Search.."/>
                                            <button type="submit"><i className="fa fa-search"></i></button>
                                        </div>
                                    </form>
                                </li>
                                <li className=" notification-list">
                                    <a className="nav-link "  href="#" ><i className="regx-message nav-icon"></i></a>
                                </li>
                           
                                <li className={`dropdown notification-list ${this.state.isNotification ? 'show' : ''}`}>
       
                                    <a className="nav-link dropdown-toggle arrow-none waves-effect" onClick={()=>this.openNotificationBar()} data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                        <i className="regx-bell noti-icon  nav-icon"></i> 
                                        <span className="badge badge-pill badge-danger noti-icon-badge">3</span>
                                    </a>
                                    <div className={`dropdown-menu dropdown-menu-right dropdown-menu-lg ${this.state.isNotification ? 'show' : ''}`}>
                                  {/* <!-- item--> */}
                                        <h6 className="dropdown-item-text">Notifications (258)</h6>
                                        <div className="slimscroll notification-item-list">
                                        {/* <!-- item-->  */}
                                            <a href="javascript:void(0);" className="dropdown-item notify-item active">
                                                <div className="notify-icon bg-success"><i className="mdi mdi-cart-outline"></i></div>
                                                <p className="notify-details">Your order is placed<span className="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                            </a>
                                     {/* <!-- item-->  */}
                                            <a href="javascript:void(0);" className="dropdown-item notify-item">
                                                <div className="notify-icon bg-warning"><i className="mdi mdi-message"></i></div>
                                                <p className="notify-details">New Message received<span className="text-muted">You have 87 unread messages</span></p>
                                            </a>
                                     {/* <!-- item-->  */}
                                            <a href="javascript:void(0);" className="dropdown-item notify-item">
                                                <div className="notify-icon bg-info"><i className="mdi mdi-martini"></i></div>
                                                <p className="notify-details">Your item is shipped<span className="text-muted">It is a long established fact that a reader will</span></p>
                                            </a>
                                     {/* <!-- item-->  */}
                                            <a href="javascript:void(0);" className="dropdown-item notify-item">
                                                <div className="notify-icon bg-primary"><i className="mdi mdi-cart-outline"></i></div>
                                                <p className="notify-details">Your order is placed<span className="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                            </a>
                                     {/* <!-- item-->  */}
                                            <a href="javascript:void(0);" className="dropdown-item notify-item">
                                                <div className="notify-icon bg-danger"><i className="mdi mdi-message"></i></div>
                                                <p className="notify-details">New Message received<span className="text-muted">You have 87 unread messages</span></p>
                                            </a>
                                        </div>
                                  {/* <!-- All-->  */}
                                        <a href="javascript:void(0);" className="dropdown-item text-center text-primary">View all <i className="fi-arrow-right"></i></a>
                                </div>
                            </li>
                            
                            <li className=" notification-list">
                                <a className="nav-link " href="#" ><i className="regx-help  nav-icon"></i></a>
                            </li>
                            <li className="dropdown notification-list">
                                    <div className={`dropdown notification-list nav-pro-img ${this.state.isOpen ? 'show' : ''}`}>
                                        <a className={`dropdown-toggle nav-link arrow-none waves-effect nav-user `} onClick={() => this.openNavBar()} data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                        <img src={DummyUser} alt="user" className="rounded-circle"/>
                                    </a>
                                    <div className={`dropdown-menu dropdown-menu-right profile-dropdown ${this.state.isOpen ? 'show' : ''}`}>
                                         {/* <!-- item-->  */}
                                        <a className="dropdown-item" href="#">
                                            <i className="mdi mdi-account-circle m-r-5"></i> Profile
                                        </a> 
                                        <a className="dropdown-item" href="#">
                                            <i className="mdi mdi-wallet m-r-5"></i> My Wallet
                                        </a> 
                                        <a className="dropdown-item d-block" href="#">
                                            <span className="badge badge-success float-right">11</span>
                                            <i className="mdi mdi-settings m-r-5"></i> Settings
                                        </a> 
                                        <a className="dropdown-item" href="#">
                                            <i className="mdi mdi-lock-open-outline m-r-5"></i> Lock screen
                                        </a>
                                        <div className="dropdown-divider"></div>
                                            <a className="dropdown-item text-danger" onClick={() => this.handleLogout()}  href="#"><i className="mdi mdi-power text-danger"></i> Logout</a>
                                  </div>
                               </div>
                            </li>
                            <li className="menu-item">
                               {/* <!-- Mobile menu toggle-->  */}
                               <a className="navbar-toggle nav-link" id="mobileToggle">
                                  <div className="lines"><span></span> <span></span> <span></span></div>
                               </a>
                               {/* <!-- End mobile menu toggle--> */}
                            </li>
                         </ul>
                      </div>
                      {/* <!-- end menu-extras --> */}
                      <div className="clearfix"></div>
                   </div>
                   {/* <!-- end container --> */}
                </div>
                {/* <!-- end topbar-main --> */}
       
               
                {/* <!-- MENU Start --> */}
                <div className="navbar-custom">
                    <div className="container-fluid">
                        <div id="navigation">
                         {/* <!-- Navigation Menu--> */}
                            <ul className="navigation-menu">                                                            
                                <li className={`has-submenu ${[
                              "accuracyReport",
                              "accuracyInstrument",
                              "pricingData",
                              "personnelData"
                                ].indexOf(this.state.active) >= 0 ? 'active' : null }`}><a href="#">Accuracy</a>
                                    <ul className="submenu">                                                                
                                        <li className={`${this.state.active == 'accuracyReport' ? 'active' : ''}`}><a href="/accuracy-report" >Reporting Eligibility</a></li>
                                        <li className={`${this.state.active == 'accuracyInstrument' ? 'active' : ''}`}><a href="/instrument-data" >Instrument Data</a></li>
                                        <li className={`${this.state.active == 'pricingData' ? 'active' : ''}`}><a href="/accuracy-pricing-data">Pricing Data </a></li>
                                        <li className={`${this.state.active == 'personnelData' ? 'active' : ''}`}><a href="/accuracy-personnel-data">Personnel Data </a></li>
                                    </ul>
                                </li>
                                <li className={`has-submenu ${[
                              "firmArm",
                              "firmArmNca"
                                ].indexOf(this.state.active) >= 0 ? 'active' : null }`}><a href="#">Completeness</a>
                                    <ul className="submenu">
                                        <li className={`${this.state.active == 'firmArm' ? 'active' : ''}`}>
                                            <a href="/firm-arm">Firm-ARM Reconciliation</a>
                                        </li>
                                        <li className={`${this.state.active == 'firmArmNca' ? 'active' : ''}`}>
                                            <a href="/firm-arm-nca">Firm-ARM-NCA Reconciliation</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="/data-import" className={`${this.state.active == 'data-import' ? 'active' : ''}`}>Data Import</a></li>
                                <li className="has-submenu">
                                    <a href="/insights" className={`${this.state.active == 'insights' ? 'active' : ''}`} ><span>Insights</span></a>
                                </li>
                            <li className="has-submenu"><a href="backreporting.html">Back Reporting</a> </li>
                            
                         </ul>
                         {/* <!-- End navigation menu --> */}
                      </div>
                      {/* <!-- end navigation --> */}
                   </div>
                   {/* <!-- end container-fluid --> */}
                </div>
                {/* <!-- end navbar-custom --> */}
             </header>
        </div>

    )
    }
};


export default Header;