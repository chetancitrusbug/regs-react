import React, { Component } from 'react';
import api from '../api.js'
import axios, { post } from 'axios'
import { Button, ButtonToolbar, Modal } from 'react-bootstrap';

class Filter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
            isNotification: false,
            active: this.props.activeClass,
            showModal: false,
            showModalClass: '',
            userData: JSON.parse(sessionStorage.getItem('userData')),
            clientList: [],
            clientListNext: [],
            listFiles: [],
            listFile: 'false',
            clientType: [],
            ClientTypeName: '',
            login: true,
            to: '',
            from: '',
            loading: false,
            transpose: true,
            alltrancation: true,
            refresh: true,
            showFocusModalClass: '',
            showFocusModal: false,
            showMatchModalClass: '',
            showMatchModal: '',
            modalOpenForOther: false,
        };
        this.handleShowModal = this.handleShowModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.fileUpload = this.fileUpload.bind(this);
        this.handleFocusCloseModal = this.handleFocusCloseModal.bind(this);
        this.handleFocusModal = this.handleFocusModal.bind(this);
        this.generateRowsWhenTranspose = this.generateRowsWhenTranspose.bind(this);
        this.handleMatchCloseModal = this.handleMatchCloseModal.bind(this);
        this.handleMatchModal = this.handleMatchModal.bind(this);
        this.handleMatch = this.handleMatch.bind(this);
        this.tableForMatch = this.tableForMatch.bind(this);
    }

    componentWillMount() {
        this.setState({ loading: false });
        let url = api.url + api.getClientType + '?api_token=' + this.state.userData.API_Token;
        // let url = api.url + api.getClient + '?api_token=' + this.state.userData.API_Token;

        // axios.get(url).then(response => {
        //     this.setState({ loading: true });
        //     this.setState({
        //         clientList: response.data.result,
        //         clientListNext: response.data.result,
        //     });
        // }).catch(error => {
        //     console.log(error);
        // });
        axios.get(url).then(response => {
            console.log(response);
            this.setState({ loading: true });
            this.setState({
                clientType: response.data.result
            });
        }).catch(error => {
            console.log(error);
        });
    }

    // getClient(field,clientId){
    //     //console.log(field);
    //     this.setState({ loading: false });
    //     if (field == 'from' || field == 'to'){
    //         let url = api.url + api.getClient + '?api_token=' + this.state.userData.API_Token +'&clientId='+clientId;
    //         axios.get(url).then(response => {
    //             if (field == 'from'){
    //                 this.setState({
    //                     clientListNext: response.data.result,
    //                 });
    //             }else if(field == 'to'){
    //                 this.setState({
    //                     clientList: response.data.result,
    //                 });
    //             }
    //             this.setState({ loading: true });
    //         }).catch(error => {
    //             console.log(error);
    //         });
    //     }
    // }

    openNavBar = () => {
        this.setState({ isOpen: !this.state.isOpen });
    }
    openNotificationBar = () => {
        this.setState({ isNotification: !this.state.isNotification });
    }

    handleCloseModal() {

        this.setState({ showModal: false, showModalClass: '' });
    }

    handleShowModal() {
        this.setState({ showModal: true, showModalClass: 'show' });
    }
    handleFocusCloseModal() {
        this.setState({ showFocusModal: false, showFocusModalClass: '', modalOpenForOther: false });
    }
    handleFocusModal() {
        this.setState({ showFocusModal: true, showFocusModalClass: 'show', modalOpenForOther: true });
    }
    handleMatchCloseModal() {
        this.setState({ showMatchModal: false, showMatchModalClass: '', modalOpenForOther: false });
    }
    handleMatchModal() {
        let idArr = [];
        idArr = this.props.inputVal.map(id => id);
        let idArrForArm = [];
        idArrForArm = this.props.inputValForArm.map(id => id);
        if (idArr.length === 0 && idArrForArm.length === 0) {
            alert('Select Data From Both Table');
        }
        else if (idArr.length !== idArrForArm.length) {
            alert('Must Select Atleast Single Data From Both Table');
        }
        else if (idArr.length > 1 || idArrForArm.length > 1) {
            alert('Must Select Only One Data From Both Table');
        }
        else {
            this.setState({ showMatchModal: true, showMatchModalClass: 'show', modalOpenForOther: true });
        }

    }

    handleUserInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        var alldata = this.state;
        alldata[name] = value
        //this.getClient(e.target.name, e.target.value)
        this.setState(alldata);
    }
    onFormSubmit(e) {
        e.preventDefault() // Stop form submit
        this.fileUpload(this.state.file);
    }

    onChange(e) {
        this.setState({ file: e.target.files[0] })
    }

    fileUpload(file) {
        var formData = new FormData();
        formData.append("file", file);
        console.log(sessionStorage.getItem('userData'))
        formData.append("username", JSON.parse(sessionStorage.getItem('userData')).UserName);
        formData.append("userid", JSON.parse(sessionStorage.getItem('userData')).UserID);
        formData.append("api_token", this.state.userData.API_Token);
        formData.append("recouncilationType", 'Firm-ARM');//this.state.recouncilationType);
        formData.append("ClientTypeName", this.state.ClientTypeName);
        // if (this.state.from != '' && this.state.to != '') {
        // if (this.state.from === this.state.to) {
        //     alert('Please Select different CLients from Dropdown.')
        //     return false;
        // }
        let url = api.url + api.uploadFiles
        axios.post(url, formData, {
            headers: { 'Content-Type': 'multipart/form-data;text/html' }
        }).then(response => {
            //                this.handleList();
            if (response.data.status) {
                alert('File Uploaded')
                this.handleCloseModal();
            } else {
                alert(response.data.message)
            }
        }).catch(error => {
            console.log(error);
        });
        // } else {
        //     alert('Please Select Clients From  the Dropdown')
        // }
    }
    handleRefresh(e) {
        const target = e.target;
        this.setState({ refresh: target.value });
        this.props.onRefreshClick(this.state.refresh);
        if (this.state.refresh === true) {
            this.setState({ refresh: false });
            this.props.onRefreshClick(this.state.refresh);
        } else {
            this.setState({ refresh: true });
            this.props.onRefreshClick(this.state.refresh);
        }
    }
    handleTrans(e) {
        const target = e.target;
        this.setState({ transpose: target.value });
        this.props.ontransClick(this.state.transpose);
        if (this.state.transpose === true) {
            this.setState({ transpose: false });
            this.props.ontransClick(this.state.transpose);
        } else {
            this.setState({ transpose: true });
            this.props.ontransClick(this.state.transpose);
        }
    }
    handleAllTransaction(e) {
        const target = e.target;
        this.setState({ alltrancation: target.value });
        this.props.onTransactionclick(this.state.alltrancation);
        if (this.state.alltrancation === true) {
            this.setState({ alltrancation: false });
            this.props.onTransactionclick(this.state.alltrancation);
        } else {
            this.setState({ alltrancation: true });
            this.props.onTransactionclick(this.state.alltrancation);
        }
    }
    getanotherColumnTransposeForArm(column, status) {
        if (this.state.modalOpenForOther){
            let columnName = column;
            let columns = [];
            var columnsRecouncilationmap = this.props.datarowForArm;
            let idArr = [];
            idArr = this.props.inputValForArm.map(id => id);
            if (status) {
                for (let j = 0; j < idArr.length; j++) {

                    for (let i = 0; i < columnsRecouncilationmap.length; i++) {
                        if (idArr[j] === `column_${i}`) {
                            columns.push(<td key={`col_${i}`}>{columnsRecouncilationmap[i][columnName]}</td>)
                        }
                    }
                }
            }
            return columns;
        }
    }

    generateRowsWhenTransposeForArm() {
        if (this.state.modalOpenForOther) {
            if(!(this.props.datacolForArm.length === 0 && this.props.datarowForArm.length === 0)) {
                let columns = this.props.datacolForArm
                if (!(columns.length === 0)) {
                    const rows = [];
                    rows.push(
                        
                            <tr key={`row_`}>
                                {this.getanotherColumnTransposeForArm(0, false)}
                            </tr>
                        
                    )
                    for (let i = 0; i < columns.length; i++) {
                        const value = columns[i].split('_').join(' ');
                        rows.push(
                        
                                <tr key={`row_${i}`} >
                                    <th>{value}</th>
                                    {this.getanotherColumnTransposeForArm(columns[i], true)}
                                </tr>
                        
                        );
                    }

                    return rows;
                }
            }
        }
    }
    getanotherColumnTranspose(column, status) {
        if (this.state.modalOpenForOther) {
            let columnName = column;
            let columns = [];
            var columnsRecouncilationmap = this.props.datarow;
            let idArr = [];
            idArr = this.props.inputVal.map(id => id);
            console.log(idArr);
            if (status) {
                for (let j = 0; j < idArr.length; j++) {

                    for (let i = 0; i < columnsRecouncilationmap.length; i++) {
                        if (idArr[j] === `column_${i}`) {
                            columns.push(<td key={`col_${i}`}>{columnsRecouncilationmap[i][columnName]}</td>)
                        }
                    }
                }
            }
            return columns;
        }
    }

    generateRowsWhenTranspose() {
        if (this.state.modalOpenForOther) {
            if (!(this.props.datacol.length === 0 && this.props.datarow.length === 0)){
                let columns = this.props.datacol
                if (!(columns.length === 0)) {
                    const rows = [];
                    rows.push(
                        
                            <tr key={`row_`}>
                                {this.getanotherColumnTranspose(0, false)}
                            </tr>
                        
                    )
                    for (let i = 0; i < columns.length; i++) {
                        const value = columns[i].split('_').join(' ');
                        rows.push(
                        
                                <tr key={`row_${i}`} >
                                    <th>{value}</th>
                                    {this.getanotherColumnTranspose(columns[i], true)}
                                </tr>
                        
                        );
                    }

                    return rows;
                }
            }
        }   
    }
    getanotherColumnMatch(column, status) {
        if (this.state.modalOpenForOther) {
            let columnName = column;
            let columns = [];
            var columnsRecouncilationmap = this.props.datarow;
            let idArr = [];
            idArr = this.props.inputVal.map(id => id);
            if (status) {
                for (let j = 0; j < idArr.length; j++) {

                    for (let i = 0; i < columnsRecouncilationmap.length; i++) {
                        console.log(i);
                        console.log(idArr);
                        if (idArr[j] === `column_${i}`) {
                            columns.push(<td key={`col_${i}`}>{columnsRecouncilationmap[i][columnName]}</td>)
                        }
                    }
                }
            }
            return columns;
        }
    }
    tableForMatch() {
        if (this.state.modalOpenForOther) {
            if (!(this.props.datacol.length === 0 && this.props.datarow.length === 0)){
                let columns = this.props.datacol;
                if (!(columns.length === 0)) {
                    const rows = [];
                    rows.push(
                        
                            <tr key={`row_`}>
                                {this.getanotherColumnMatch(0, false)}
                            </tr>
                        
                    )
                    for (let i = 0; i < columns.length; i++) {
                        const value = columns[i].split('_').join(' ');
                        rows.push(
                        
                                <tr key={`row_${i}`} >
                                    <th>{value}</th>
                                    {this.getanotherColumnMatch(columns[i], true)}
                                    {this.getanotherColumnTransposeForArm(columns[i], true)}


                                </tr>
                        
                        );
                    }

                    return rows;
                }
            }
        }
    }
    handleMatch() {
        let idArr = [];
        idArr = this.props.inputVal.map(id => id);
        let idArrForArm = [];
        idArrForArm = this.props.inputValForArm.map(id => id);
        if (idArr.length !== idArrForArm.length) {
            return <p>Must Select From Both Table</p>
        }
        if (idArr.length > 1 || idArrForArm.length > 1) {
            return <p>Must Select Only One Data From Both Table</p>
        } else {
            return <div>
                {this.tableForMatch()}
            </div>

        }
    }
    render() {
        let { inputVal } = this.props;
        let { datarow } = this.props;
        let { datacol } = this.props;
        let { inputValForArm } = this.props;
        let { datarowForArm } = this.props;
        let { datacolForArm } = this.props;
        // var ClientTypes = this.state.clientType.map(function (data, number) {
        //     return    <div key={number} className="radio-inline clearfix">
        //         <span for={data.ClientTypeName} className="radio-span1">{data.ClientTypeName} </span>
        //         <input type="radio" id={data.ClientTypeName}  onChange={(event) => this.handleUserInput(event)} name="ClientType" className=" form-control radio-input" value={data.ClientTypeName} />
        //     </div>

        // });
        // var allProfiles = this.state.clientList.map(function (data, number) {
        //     return <option key={number} value={data.ClientName}>{data.ClientName}</option>
        // });
        // var allClients = this.state.clientListNext.map(function (data, number) {
        //     return <option key={number} value={data.ClientName}>{data.ClientName}</option>
        // });
        return (
            <div className="col-xl-6 col-sm-6 pull-right">
                <div className="filter-search-div">
                    <div className="filter-nav-div ">
                        <div className="filt-nav-ul clearfix">
                            <ul>
                                <li>

                                    {/* <input type="file" id="file1" name="image" accept="image/*" capture style={{"display":"none"}}/> */}
                                    <span onClick={this.handleShowModal} className="ion-upload arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Upload" title="Upload"></span>

                                </li>
                                <li>

                                    <label onClick={this.handleRefresh.bind(this)}
                                        value={this.state.refresh} className="ion-refresh arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Refresh" title="Refresh"></label>

                                </li>
                                {/* <!--li><a href="#"><span className="ion-search" data-toggle="tooltip" data-placement="top" data-title="Search" title="Search"></span></a></li--> */}
                                {/* <li><a href="#"><span className="ion-android-sort" data-toggle="tooltip" data-placement="top" data-title="Sort" title="Sort"></span></a></li> */}
                                <li>

                                    <label onClick={this.handleTrans.bind(this)}
                                        value={this.state.transpose} className="ion-android-arrow-down-right arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Transpose" title="Transpose"></label>

                                </li>
                                <li>
                                    <span onClick={this.handleFocusModal} className="ion-flash arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Focus" title="Focus">
                                    </span>

                                </li>
                                <li>
                                    
                                    <span onClick={this.handleMatchModal} className="ion-magnet arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Match" title="Match"></span>
                                    
                                </li>
                                <li>

                                    <label onClick={this.handleAllTransaction.bind(this)}
                                        value={this.state.alltrancation} className="ion-social-buffer arrowpadding" data-toggle="tooltip" data-placement="top" data-title="All Transaction" title="All Transaction"></label>

                                </li>
                                <li>
                                    <span className="ion-compose arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Edit" title="Edit"></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {/* <!-- end of filter-nav-div--> */}
                    <div className="search-data-div">
                        <div className="clearfix">
                            <form>
                                <div className="search-col-1"><input type="text" className="form-control" placeholder="Search" /></div>
                                <div className="search-col-6"><button className="btn sbtn">Search</button></div>
                            </form>
                        </div>
                    </div>
                </div>
                {/* <!-- end of filter-search-div --> */}
                <Modal className={`${this.state.showModalClass}`} show={this.state.showModal} onHide={this.handleCloseModal}>
                    <Modal.Header className="modal-header1" closeButton>
                        <Modal.Title className="title-files">Upload Files</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="container">
                            <div className="demoForm">
                                {!this.state.loading ? <div><i className="fa fa-spin fa-fw fa-spinner"></i> Loding</div> :
                                    <form onSubmit={this.onFormSubmit}>
                                        {/* <div className={`form-group col-md-12 radio-root1 clearfix`}>
                                            <label className="col-md-12 padd-0 clearfix " htmlFor="email">Recouncilation Type</label>
                                            
                                            <div className="radio-inline clearfix">
                                                <span for="Firm-ARM" className="radio-span1">Firm-ARM </span> 
                                                <input type="radio" id="Firm-ARM" onChange={(event) => this.handleUserInput(event)} name="recouncilationType" className=" form-control radio-input" value="Firm-ARM" />
                                            </div>
                                            <div className="radio-inline clearfix">
                                                <span className="radio-span1" for="Firm-ARM-FCA">Firm-ARM-FCA </span>
                                                <input type="radio" id="Firm-ARM-FCA" onChange={(event) => this.handleUserInput(event)} name="recouncilationType" className=" form-control radio-input" value="Firm-ARM-FCA" />
                                            </div>

                                        </div> */}
                                        <div className="form-group clearfix">
                                            <label htmlFor="state" className="col-md-12 control-label clearfix">Client Type </label>
                                            <div className="col-md-12 clearfix">
                                                {/* {ClientTypes} */}
                                                {
                                                    this.state.clientType.map((option, i) => {
                                                        return <label key={i}>
                                                            <input
                                                                type="radio"
                                                                name="ClientTypeName"
                                                                key={i}
                                                                onChange={this.handleUserInput.bind(this)}
                                                                value={option.ClientTypeName} />
                                                            {option.ClientTypeName}
                                                        </label>
                                                    })
                                                }
                                            </div>

                                        </div>
                                        <div className={`form-group col-md-12 clearfix`}>
                                            <label className="col-md-12 padd-0 clearfix" htmlFor="email">File Upload</label>
                                            <input type="file" onChange={this.onChange} className=" form-control input-file" />
                                        </div>
                                        {/* <div className="form-group">
                                            <label htmlFor="state" className="col-md-2 control-label">From </label>
                                            <div className="col-md-6 ">
                                                <select className="form-control" defaultValue='' onChange={(event) => this.handleUserInput(event)} required="" id="from" name="from">
                                                    <option value=''>Select</option>
                                                    {allProfiles}
                                                </select>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="state" className="col-md-2 control-label">To </label>
                                            <div className="col-md-6">
                                                <select className="form-control" defaultValue='' onChange={(event) => this.handleUserInput(event)} required="" id="to" name="to">
                                                    <option value=''>Select</option>
                                                    {allClients}
                                                </select>
                                            </div>
                                        </div> */}
                                        <div className="col-md-9">
                                            <button type="submit" className="btn btn-primary ">Upload</button>
                                        </div>
                                    </form>
                                }
                                {/* <div className="cols col-md-3">
                        <a href="/forgot-password" className="btn btn-primary">Forgot Password</a>
                    </div> */}

                            </div>



                        </div>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleCloseModal}>Close</Button>
                    </Modal.Footer>
                </Modal>
                {/* focus model */}
                <Modal className={`${this.state.showFocusModalClass}`} show={this.state.showFocusModal} onHide={this.handleFocusCloseModal}>
                    <Modal.Header className="modal-header1" closeButton>
                        <Modal.Title className="title-files">Transaction</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="modal-new1">
                            <div className="row">
                                <div className="col-xl-12">
                                    <div className="card m-b-20">
                                        <div className="card-body">
                                            <h4 className="mt-0 m-b-10 header-title float-left">Firm Transaction</h4>
                                            <div>
                                                <div className="table-responsive">
                                                    <table className="fixed_header table table-striped table-bordered table-hover">
                                                        <tbody>
                                                            {this.generateRowsWhenTranspose()}
                                                        </tbody>
                                                        
                                                    </table>
                                                </div>

                                            </div>

                                            <h4 className="mt-0 m-b-10 header-title float-left">Arm Transaction</h4>
                                            <div>
                                                <div className="table-responsive">
                                                    <table className="fixed_header table table-striped table-bordered table-hover">
                                                        <tbody>
                                                            {this.generateRowsWhenTransposeForArm()}
                                                        </tbody>
                                                        
                                                    </table>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleFocusCloseModal}>Close</Button>
                    </Modal.Footer>
                </Modal>
                <Modal className={`${this.state.showMatchModalClass}`} show={this.state.showMatchModal} onHide={this.handleMatchCloseModal}>
                    <Modal.Header className="modal-header1" closeButton>
                        <Modal.Title className="title-files">Match</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                        <div className="modal-new1">
                            <div className="row">
                                <div className="col-xl-12">
                                    <div className="card m-b-20">
                                        <div className="card-body">
                                            <h4 className="mt-0 m-b-10 header-title float-left">Match Table</h4>
                                            <div>
                                                <div className="table-responsive">
                                                    <table className="fixed_header table table-striped table-bordered table-hover">
                                                        <tbody>
                                                            {this.handleMatch()}
                                                        </tbody>
                                                        
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleMatchCloseModal}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
};
export default Filter;

