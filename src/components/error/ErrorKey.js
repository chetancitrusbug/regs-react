export default {
    email: 'Email',
    password: 'Password',
    oldpassword: 'Old Password',
    newpassword: 'New Password',
    confirmNewPassword: 'Confirm New Password',
    first_name: 'First Name',
    last_name: 'Last Name',
    street_name: 'Street Name',
    post_code: 'Post Code',
    phone_mobile: 'Mobile No.', 
}