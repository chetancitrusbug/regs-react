import api from '../api.js'
import axios from 'axios'


export function LoginData(userData) {
    let BaseURL = api.localUrl;
    return new Promise((resolve, reject) =>{
        axios.post(BaseURL, userData)
        .then(function (response) {
            resolve(response);
        })
        .catch(function (error) {
            reject(error);
        });
    });
}


export function UploadFile(userData, config) {
    let BaseURL = api.localuploadUrl;
    return new Promise((resolve, reject) => {
        axios.post(BaseURL, userData, config)
            .then(function (response) {
                resolve(response);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}

export function ForgotPasswordData(postData) {
    let BaseURL = api.url + api.forgotPassword;
    return new Promise((resolve, reject) => {
        axios.post(BaseURL, postData)
            .then(function (response) {
                resolve(response.data);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}

export function ChangePasswordData(postData) {
    let BaseURL = api.url + api.changePassword;
    return new Promise((resolve, reject) => {
        axios.post(BaseURL, postData)
            .then(function (response) {
                resolve(response.data);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}

export function CustomerData(postData) {
    let BaseURL = api.url + api.customerList+ '?page=' + postData.page;
    return new Promise((resolve, reject) => {
        axios.post(BaseURL, postData)
            .then(function (response) {
                resolve(response);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}


export function FetchCustomerData(postData) {
    let BaseURL = api.url + api.customerDetail + '?api_token=' + postData.api_token + '&customer_id=' + postData.customer_id;
    return new Promise((resolve, reject) => {
        axios.get(BaseURL)
            .then(function (response) {
                resolve(response.data);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}

export function CustomerEditPostData(postData) {
    let BaseURL = api.url + api.customerUpdate ;
    return new Promise((resolve, reject) => {
        axios.post(BaseURL,postData)
            .then(function (response) {
                resolve(response.data);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}

export function FetchProvidersData(postData){
    let BaseURL = api.url + api.getProviders + '?api_token=' + postData.api_token;
    return new Promise((resolve, reject) => {
        axios.get(BaseURL)
            .then(function (response) {
                resolve(response.data);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}

export function FetchPackageList(postData) {
    let BaseURL = api.url + api.fetchOffers + '?api_token=' + postData.api_token + '&min=' + postData.min + '&max=' + postData.max + '&customer_id=' + postData.customer_id;
    return new Promise((resolve, reject) => {
        axios.get(BaseURL)
            .then(function (response) {
                resolve(response.data);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}

export function CreateOfferForCustomer(postData) {
    let BaseURL = api.url + api.createoffer;
    return new Promise((resolve, reject) => {
        axios.get(BaseURL)
            .then(function (response) {
                resolve(response.data);
            })
            .catch(function (error) {
                reject(error);
            });
    });
}
